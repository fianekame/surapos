-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 13, 2019 at 06:03 
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `poskelud`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `idadmin` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '3333'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`idadmin`, `username`, `password`, `type`) VALUES
(1, 'admin', '03f7f7198958ffbda01db956d15f134a', 3333);

-- --------------------------------------------------------

--
-- Table structure for table `bahan`
--

CREATE TABLE `bahan` (
  `idbahan` int(11) NOT NULL,
  `idjenisbahan` int(11) NOT NULL,
  `idstore` int(11) NOT NULL,
  `namabahan` varchar(64) NOT NULL,
  `stokbahan` int(11) NOT NULL,
  `satuan` varchar(64) NOT NULL,
  `keteranganbahan` text NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bahan`
--

INSERT INTO `bahan` (`idbahan`, `idjenisbahan`, `idstore`, `namabahan`, `stokbahan`, `satuan`, `keteranganbahan`, `update_at`) VALUES
(1, 1, 1, 'Susu UHT Diamond', 8000, 'Liter', 'Susu Fresh Milk 1', '2019-05-30 07:43:10'),
(2, 1, 1, 'House Blend Robusta', 1000, 'Gram', 'Robusta Untuk Kopi	', '2019-05-30 07:42:45'),
(3, 1, 1, 'Susu UHT Diamond 1', 5000, 'Liter', 'Susu Fresh Milk', '2019-03-05 13:15:08'),
(4, 1, 1, 'House Blend Robusta 1', 1000, 'Gram', 'Robusta Untuk Kopi	', '2019-03-05 13:15:08');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `idbarang` int(11) NOT NULL,
  `idstore` int(11) NOT NULL,
  `namabarang` varchar(64) NOT NULL,
  `lokasi` varchar(64) NOT NULL,
  `keteranganbarang` text NOT NULL,
  `kondisi` varchar(64) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`idbarang`, `idstore`, `namabarang`, `lokasi`, `keteranganbarang`, `kondisi`, `jumlah`, `update_at`) VALUES
(1, 1, 'Mesin Kopi', 'Bar', 'Merk Cara Perawatan Dan Keternagna Lainnya', 'Layak Pakai', 1, '2019-03-05 13:47:29'),
(2, 1, 'Grinder Mazzer', 'Bar', 'Merk Cara Perawatan Dan Keternagna Lainnya', 'Layak Pakai', 1, '2019-03-05 13:47:29'),
(3, 1, 'Grinder Orange Manual', 'Bar', 'Merk Cara Perawatan Dan Keternagna Lainnya', 'Layak Pakai', 1, '2019-03-05 13:47:29');

-- --------------------------------------------------------

--
-- Table structure for table `belanjabulanan`
--

CREATE TABLE `belanjabulanan` (
  `idbelanjabulanan` int(11) NOT NULL,
  `idbaranggudang` int(11) NOT NULL,
  `idstore` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `jumlahbeli` int(11) NOT NULL,
  `hargasatuan` int(11) DEFAULT NULL,
  `totalharga` int(11) NOT NULL,
  `tglbeli` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `keteranganbelanjabulanan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `belanjabulanan`
--

INSERT INTO `belanjabulanan` (`idbelanjabulanan`, `idbaranggudang`, `idstore`, `iduser`, `jumlahbeli`, `hargasatuan`, `totalharga`, `tglbeli`, `keteranganbelanjabulanan`) VALUES
(1, 1, 1, 1, 1000, 50, 50000, '2019-03-25 04:24:28', 'asffsfvsvfsfs fs sf s fa fqwwfas fa fas fsf sf sddfs dfas fasf sf'),
(3, 4, 1, 1, 2000, 0, 350000, '2019-03-25 04:24:32', 'Tes123');

--
-- Triggers `belanjabulanan`
--
DELIMITER $$
CREATE TRIGGER `belanja_bulanan` AFTER INSERT ON `belanjabulanan` FOR EACH ROW UPDATE gudang
SET stokbarang = stokbarang + NEW.jumlahbeli
WHERE
idbaranggudang = NEW.idbaranggudang
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `belanjabulanan_cancel` AFTER DELETE ON `belanjabulanan` FOR EACH ROW UPDATE gudang
SET stokbarang = stokbarang - OLD.jumlahbeli
WHERE
idbaranggudang = OLD.idbaranggudang
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `belanjaharian`
--

CREATE TABLE `belanjaharian` (
  `idbelanjaharian` int(11) NOT NULL,
  `idbahan` int(11) NOT NULL,
  `idstore` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `jumlahbeli` int(11) NOT NULL,
  `hargasatuan` int(11) DEFAULT NULL,
  `total` int(11) NOT NULL,
  `tglbeli` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `keteranganbelanjaharian` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `belanjaharian`
--

INSERT INTO `belanjaharian` (`idbelanjaharian`, `idbahan`, `idstore`, `iduser`, `jumlahbeli`, `hargasatuan`, `total`, `tglbeli`, `keteranganbelanjaharian`) VALUES
(1, 1, 1, 1, 1000, 15000, 15000, '2019-03-25 04:24:47', 'Belanja ini didobel akrena kemaren ada kendala');

--
-- Triggers `belanjaharian`
--
DELIMITER $$
CREATE TRIGGER `belanja_cancel` AFTER DELETE ON `belanjaharian` FOR EACH ROW UPDATE bahan
SET stokbahan = stokbahan - OLD.jumlahbeli
WHERE
idbahan = OLD.idbahan
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `belanja_harian` AFTER INSERT ON `belanjaharian` FOR EACH ROW UPDATE bahan
SET stokbahan = stokbahan + NEW.jumlahbeli
WHERE
idbahan = NEW.idbahan
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `casier`
--

CREATE TABLE `casier` (
  `idcasier` int(11) NOT NULL,
  `kodekasir` int(11) NOT NULL,
  `tgloperasi` datetime NOT NULL,
  `jambuka` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `jamtutup` timestamp NULL DEFAULT NULL,
  `uangmodal` int(11) NOT NULL,
  `catatan` varchar(256) NOT NULL,
  `idstore` int(11) NOT NULL,
  `statuskasir` int(1) NOT NULL DEFAULT '0' COMMENT '0 itu open 1 itu close'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gudang`
--

CREATE TABLE `gudang` (
  `idbaranggudang` int(11) NOT NULL,
  `idstore` int(11) NOT NULL,
  `namabaranggudang` varchar(64) NOT NULL,
  `satuan` varchar(12) NOT NULL,
  `stokbarang` int(11) NOT NULL,
  `keteranganbahangudang` text NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gudang`
--

INSERT INTO `gudang` (`idbaranggudang`, `idstore`, `namabaranggudang`, `satuan`, `stokbarang`, `keteranganbahangudang`, `update_at`) VALUES
(1, 1, 'Susu UHT Diamon', 'Liter', 14000, 'Susu Fresh Milk 1', '2019-03-05 13:13:04'),
(2, 1, 'House Blend', 'Gram', 5000, 'Robusta Untuk Kopi', '2019-03-05 13:13:04'),
(3, 1, 'Susu UHT Diamon', 'Liter', 10000, 'Susu Fresh Milk', '2019-03-05 13:13:04'),
(4, 1, 'House Blend', 'Gram', 4000, 'Robusta Untuk Kopi', '2019-03-05 13:13:04');

-- --------------------------------------------------------

--
-- Table structure for table `jenisbahan`
--

CREATE TABLE `jenisbahan` (
  `idjenisbahan` int(11) NOT NULL,
  `idstore` int(11) NOT NULL,
  `namajenis` varchar(64) NOT NULL,
  `keteranganjenisbahan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenisbahan`
--

INSERT INTO `jenisbahan` (`idjenisbahan`, `idstore`, `namajenis`, `keteranganjenisbahan`) VALUES
(1, 1, 'Gudang', 'Jenis Bahan Yang Ada Pada Gudang'),
(2, 1, 'Bar', 'Jenis Bahan Yang Ada Pada Bar');

-- --------------------------------------------------------

--
-- Table structure for table `kategoriproduk`
--

CREATE TABLE `kategoriproduk` (
  `idkategoriproduk` int(11) NOT NULL,
  `idstore` int(11) NOT NULL,
  `namakategori` varchar(64) NOT NULL,
  `keterangankategoriproduk` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategoriproduk`
--

INSERT INTO `kategoriproduk` (`idkategoriproduk`, `idstore`, `namakategori`, `keterangankategoriproduk`) VALUES
(1, 1, 'Snack & Bevarage', 'Snak ringan pendamping minuman'),
(2, 1, 'Flavour', 'Sejenin minuman ayng sejnis ');

-- --------------------------------------------------------

--
-- Table structure for table `ordergudang`
--

CREATE TABLE `ordergudang` (
  `idordergudang` int(11) NOT NULL,
  `idstore` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idbaranggudang` int(11) NOT NULL,
  `jumlahorder` int(11) NOT NULL,
  `tglorder` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `keteranganorder` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ordergudang`
--

INSERT INTO `ordergudang` (`idordergudang`, `idstore`, `iduser`, `idbaranggudang`, `jumlahorder`, `tglorder`, `keteranganorder`) VALUES
(12, 1, 1, 4, 1000, '2019-03-17 15:01:51', 'keperluan'),
(13, 1, 1, 4, 2000, '2019-03-17 15:02:29', 'testes'),
(14, 1, 1, 3, 1200, '2019-03-18 09:58:17', 'rers'),
(15, 1, 1, 3, 800, '2019-03-18 09:59:12', 'tesss');

--
-- Triggers `ordergudang`
--
DELIMITER $$
CREATE TRIGGER `t_order_batalkan` AFTER DELETE ON `ordergudang` FOR EACH ROW UPDATE gudang
SET stokbarang = stokbarang + OLD.jumlahorder
WHERE
idbaranggudang = OLD.idbaranggudang
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `t_order_gudang_minus` AFTER INSERT ON `ordergudang` FOR EACH ROW UPDATE gudang
SET stokbarang = stokbarang - NEW.jumlahorder
WHERE
idbaranggudang = NEW.idbaranggudang
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pemasukan`
--

CREATE TABLE `pemasukan` (
  `idpemasukan` int(11) NOT NULL,
  `idstore` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `tglpemasukan` date NOT NULL,
  `keteranganpemasukan` text NOT NULL,
  `jumlahpemasukan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemasukan`
--

INSERT INTO `pemasukan` (`idpemasukan`, `idstore`, `iduser`, `tglpemasukan`, `keteranganpemasukan`, `jumlahpemasukan`) VALUES
(1, 1, 1, '2019-03-07', 'Pemasukan Acara Erci 11', 350000),
(2, 1, 1, '2019-03-07', 'Penjualan Green Bean', 25000);

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran`
--

CREATE TABLE `pengeluaran` (
  `idpengeluaran` int(11) NOT NULL,
  `idstore` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `keteranganpengeluaran` text NOT NULL,
  `totalkeluar` int(11) NOT NULL,
  `tglpengeluaran` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran`
--

INSERT INTO `pengeluaran` (`idpengeluaran`, `idstore`, `iduser`, `keteranganpengeluaran`, `totalkeluar`, `tglpengeluaran`) VALUES
(2, 1, 1, 'daf ad ad ad asd asdmna sj dj asjd ajs dja dkja nkjdn akjd jka d jadkj', 22000, '2019-03-25 06:38:41');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `idproduk` int(11) NOT NULL,
  `kodeproduk` varchar(32) NOT NULL,
  `namaproduk` varchar(64) NOT NULL,
  `idkategoriproduk` int(11) NOT NULL,
  `idstore` int(11) NOT NULL,
  `hargaproduksi` varchar(64) NOT NULL,
  `hargajual` varchar(64) NOT NULL,
  `gambar` varchar(64) NOT NULL,
  `keteranganproduk` text NOT NULL,
  `stokan` int(1) NOT NULL COMMENT '0 stok 1 tidak',
  `jumlahstok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`idproduk`, `kodeproduk`, `namaproduk`, `idkategoriproduk`, `idstore`, `hargaproduksi`, `hargajual`, `gambar`, `keteranganproduk`, `stokan`, `jumlahstok`) VALUES
(1, 'KLD-1', 'Avocado Float', 1, 1, '8000', '12000', 'minuman.jpeg', 'Avocado adalah blablablab', 1, 0),
(2, 'KLD-2', 'Avocado Coffe Float', 2, 1, '8000', '14000', 'min1uman.jpeg', '    Avocado adalah blablablab        ', 1, 0),
(3, 'KLD-3', 'Kentang', 1, 1, '5000', '10000', '03_18_04_2019_03_06_Kentang.jpg', '       minumsand asidnasidn diasdnj anduiansdjaudnasudn sufh suihfu shfiuha uifhasi bfiuah ifhai ufhhaiuhfiua hufjab ifuahif        ', 0, 15);

-- --------------------------------------------------------

--
-- Table structure for table `produkdetail`
--

CREATE TABLE `produkdetail` (
  `idpd` int(11) NOT NULL,
  `idproduk` int(11) NOT NULL,
  `idbahan` int(11) NOT NULL,
  `jumlahbahan` int(11) NOT NULL,
  `keteranganprodukdetail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produkdetail`
--

INSERT INTO `produkdetail` (`idpd`, `idproduk`, `idbahan`, `jumlahbahan`, `keteranganprodukdetail`) VALUES
(1, 1, 1, 30, 'Penggunaan Bahan'),
(2, 1, 2, 30, 'Penggunaan Bahan'),
(8, 2, 1, 30, 'dasfvsfa fsa asd fasd fasd fsd fdsf'),
(9, 2, 2, 15, 'dasfvsfa fsa asd fasd fasd fsd fdsf');

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `idstore` int(11) NOT NULL,
  `namastore` varchar(64) NOT NULL,
  `logo` varchar(128) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `alamat` varchar(64) NOT NULL,
  `keteranganstore` varchar(256) NOT NULL,
  `statusstore` int(1) NOT NULL COMMENT '0 aktif 1 tidak'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`idstore`, `namastore`, `logo`, `phone`, `alamat`, `keteranganstore`, `statusstore`) VALUES
(1, 'Kopi Kelud Store', 'kelud.jpg', '087750731997', 'Jalan Kelud No 07 Mlajah Bangkalan', 'Sebuah warung kopi adalah merujuk kepada sebuah organisasi yang secara esensial menyediakan kopi atau minuman panas lainnya. Ia berbagi beberapa dari ciri-ciri sebuah bar', 0),
(2, 'Kopi Kelud Giras', 'kelud.jpg', '087750731997', 'Jalan Kelud No 07 Burneh Bangkalan', 'Sebuah warung kopi adalah merujuk kepada sebuah organisasi yang secara esensial menyediakan kopi atau minuman panas lainnya. Ia berbagi beberapa dari ciri-ciri sebuah bar', 0),
(3, 'Kopi Kelud SBY', 'kelud.jpg', '087750731997', 'Jalan Kelud No 07 Keputeran Surabaya', 'Sebuah warung kopi adalah merujuk kepada sebuah organisasi yang secara esensial menyediakan kopi atau minuman panas lainnya. Ia berbagi beberapa dari ciri-ciri sebuah bar', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `idtrans` int(11) NOT NULL,
  `idstore` int(11) NOT NULL,
  `idcasier` int(11) NOT NULL,
  `tglorder` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `jumlahorder` int(11) NOT NULL,
  `hargatotal` int(11) NOT NULL,
  `jumlahbayar` int(11) NOT NULL,
  `status` int(1) NOT NULL COMMENT '0 belumbayar 1 lunas 2 utang',
  `iduser` int(11) NOT NULL,
  `keterangantransaksi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transdetail`
--

CREATE TABLE `transdetail` (
  `iddetail` int(11) NOT NULL,
  `idtrans` int(11) NOT NULL,
  `idproduk` int(11) NOT NULL,
  `jmlbeli` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `idstore` int(11) NOT NULL,
  `nadep` varchar(64) NOT NULL,
  `nabel` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `aktif` int(1) NOT NULL COMMENT '0 aktif 1 tidak',
  `password` varchar(64) NOT NULL,
  `foto` varchar(256) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `jabatan` varchar(64) NOT NULL,
  `statususer` int(1) NOT NULL DEFAULT '1' COMMENT '0  admin 1 pegawai'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`iduser`, `idstore`, `nadep`, `nabel`, `email`, `aktif`, `password`, `foto`, `phone`, `jabatan`, `statususer`) VALUES
(1, 1, 'Sofian', 'Eka Sandra', 'sofian@kopikelud.com', 0, 'dd1a3add72e2c3a4522d9b47e95949bc', '06_21_58_2019_03_04_Sofian.jpg', '087750731997', 'Backoffice Administrator', 0),
(2, 1, 'Adhenk', 'Darwis', 'adhenk@kopikelud.com', 0, 'e3620fe35e79575e2b8e092a1d6586af', 'adhenk.jpg', '087750731997', 'Founder', 0),
(3, 1, 'Vici', 'Vici', 'vici@kopikelud.com', 0, '695972c29096ac03cf40324d307f93e8', '06_24_46_2019_03_04_Vici.jpg', '087750731997', 'Kepala Bar', 1),
(4, 1, 'Boy', 'Boy', 'boy@kopikelud.com', 0, 'f93110b84819015d07387f6d3211cca3', 'boy.jpg', '087750731997', 'Barista', 1),
(5, 1, 'Amin', 'Amin', 'amin@kopikelud.com', 0, '0937ffb47bf01895f03112902d3dca18', '06_24_55_2019_03_04_Amin.jpg', '087750731997', 'Barista', 1),
(6, 1, 'Aril', 'Aril', 'aril@kopikelud.com', 0, '5614633ba7d968f5126bfdee48467e0b', '06_25_07_2019_03_04_Aril.jpg', '087750731997', 'Barista', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`idadmin`);

--
-- Indexes for table `bahan`
--
ALTER TABLE `bahan`
  ADD PRIMARY KEY (`idbahan`),
  ADD KEY `bahan_ibfk_1` (`idjenisbahan`),
  ADD KEY `idstore` (`idstore`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`idbarang`),
  ADD KEY `idstore` (`idstore`);

--
-- Indexes for table `belanjabulanan`
--
ALTER TABLE `belanjabulanan`
  ADD PRIMARY KEY (`idbelanjabulanan`),
  ADD KEY `idbaranggudang` (`idbaranggudang`),
  ADD KEY `idstore` (`idstore`),
  ADD KEY `iduser` (`iduser`);

--
-- Indexes for table `belanjaharian`
--
ALTER TABLE `belanjaharian`
  ADD PRIMARY KEY (`idbelanjaharian`),
  ADD KEY `idbahan` (`idbahan`),
  ADD KEY `idstore` (`idstore`),
  ADD KEY `iduser` (`iduser`);

--
-- Indexes for table `casier`
--
ALTER TABLE `casier`
  ADD PRIMARY KEY (`idcasier`),
  ADD UNIQUE KEY `kodekasir` (`kodekasir`),
  ADD KEY `idstore` (`idstore`);

--
-- Indexes for table `gudang`
--
ALTER TABLE `gudang`
  ADD PRIMARY KEY (`idbaranggudang`),
  ADD KEY `idstore` (`idstore`);

--
-- Indexes for table `jenisbahan`
--
ALTER TABLE `jenisbahan`
  ADD PRIMARY KEY (`idjenisbahan`),
  ADD KEY `idstore` (`idstore`);

--
-- Indexes for table `kategoriproduk`
--
ALTER TABLE `kategoriproduk`
  ADD PRIMARY KEY (`idkategoriproduk`),
  ADD KEY `idstore` (`idstore`);

--
-- Indexes for table `ordergudang`
--
ALTER TABLE `ordergudang`
  ADD PRIMARY KEY (`idordergudang`),
  ADD KEY `idstore` (`idstore`),
  ADD KEY `iduser` (`iduser`),
  ADD KEY `idbaranggudang` (`idbaranggudang`);

--
-- Indexes for table `pemasukan`
--
ALTER TABLE `pemasukan`
  ADD PRIMARY KEY (`idpemasukan`),
  ADD KEY `idstore` (`idstore`),
  ADD KEY `iduser` (`iduser`);

--
-- Indexes for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  ADD PRIMARY KEY (`idpengeluaran`),
  ADD KEY `idstore` (`idstore`),
  ADD KEY `iduser` (`iduser`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`idproduk`),
  ADD KEY `idkategoriproduk` (`idkategoriproduk`),
  ADD KEY `idstore` (`idstore`);

--
-- Indexes for table `produkdetail`
--
ALTER TABLE `produkdetail`
  ADD PRIMARY KEY (`idpd`),
  ADD KEY `idproduk` (`idproduk`),
  ADD KEY `idbahan` (`idbahan`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`idstore`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`idtrans`),
  ADD KEY `idstore` (`idstore`),
  ADD KEY `idcasier` (`idcasier`),
  ADD KEY `iduser` (`iduser`);

--
-- Indexes for table `transdetail`
--
ALTER TABLE `transdetail`
  ADD PRIMARY KEY (`iddetail`),
  ADD KEY `idtrans` (`idtrans`),
  ADD KEY `idproduk` (`idproduk`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`),
  ADD KEY `idstore` (`idstore`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `idadmin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bahan`
--
ALTER TABLE `bahan`
  MODIFY `idbahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `idbarang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `belanjabulanan`
--
ALTER TABLE `belanjabulanan`
  MODIFY `idbelanjabulanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `belanjaharian`
--
ALTER TABLE `belanjaharian`
  MODIFY `idbelanjaharian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `casier`
--
ALTER TABLE `casier`
  MODIFY `idcasier` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gudang`
--
ALTER TABLE `gudang`
  MODIFY `idbaranggudang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jenisbahan`
--
ALTER TABLE `jenisbahan`
  MODIFY `idjenisbahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kategoriproduk`
--
ALTER TABLE `kategoriproduk`
  MODIFY `idkategoriproduk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ordergudang`
--
ALTER TABLE `ordergudang`
  MODIFY `idordergudang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `pemasukan`
--
ALTER TABLE `pemasukan`
  MODIFY `idpemasukan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  MODIFY `idpengeluaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `idproduk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `produkdetail`
--
ALTER TABLE `produkdetail`
  MODIFY `idpd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `idstore` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `idtrans` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transdetail`
--
ALTER TABLE `transdetail`
  MODIFY `iddetail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bahan`
--
ALTER TABLE `bahan`
  ADD CONSTRAINT `bahan_ibfk_1` FOREIGN KEY (`idjenisbahan`) REFERENCES `jenisbahan` (`idjenisbahan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bahan_ibfk_2` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `belanjabulanan`
--
ALTER TABLE `belanjabulanan`
  ADD CONSTRAINT `belanjabulanan_ibfk_1` FOREIGN KEY (`idbaranggudang`) REFERENCES `gudang` (`idbaranggudang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `belanjabulanan_ibfk_2` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `belanjabulanan_ibfk_3` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`);

--
-- Constraints for table `belanjaharian`
--
ALTER TABLE `belanjaharian`
  ADD CONSTRAINT `belanjaharian_ibfk_1` FOREIGN KEY (`idbahan`) REFERENCES `bahan` (`idbahan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `belanjaharian_ibfk_2` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `belanjaharian_ibfk_3` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`);

--
-- Constraints for table `casier`
--
ALTER TABLE `casier`
  ADD CONSTRAINT `casier_ibfk_1` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `gudang`
--
ALTER TABLE `gudang`
  ADD CONSTRAINT `gudang_ibfk_1` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jenisbahan`
--
ALTER TABLE `jenisbahan`
  ADD CONSTRAINT `jenisbahan_ibfk_1` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`);

--
-- Constraints for table `kategoriproduk`
--
ALTER TABLE `kategoriproduk`
  ADD CONSTRAINT `kategoriproduk_ibfk_1` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ordergudang`
--
ALTER TABLE `ordergudang`
  ADD CONSTRAINT `ordergudang_ibfk_1` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ordergudang_ibfk_2` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ordergudang_ibfk_3` FOREIGN KEY (`idbaranggudang`) REFERENCES `gudang` (`idbaranggudang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pemasukan`
--
ALTER TABLE `pemasukan`
  ADD CONSTRAINT `pemasukan_ibfk_1` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pemasukan_ibfk_2` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`);

--
-- Constraints for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  ADD CONSTRAINT `pengeluaran_ibfk_1` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengeluaran_ibfk_2` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`);

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `produk_ibfk_1` FOREIGN KEY (`idkategoriproduk`) REFERENCES `kategoriproduk` (`idkategoriproduk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `produk_ibfk_2` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `produkdetail`
--
ALTER TABLE `produkdetail`
  ADD CONSTRAINT `produkdetail_ibfk_1` FOREIGN KEY (`idproduk`) REFERENCES `produk` (`idproduk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `produkdetail_ibfk_2` FOREIGN KEY (`idbahan`) REFERENCES `bahan` (`idbahan`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`),
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`idcasier`) REFERENCES `casier` (`idcasier`),
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`);

--
-- Constraints for table `transdetail`
--
ALTER TABLE `transdetail`
  ADD CONSTRAINT `transdetail_ibfk_1` FOREIGN KEY (`idtrans`) REFERENCES `transaksi` (`idtrans`),
  ADD CONSTRAINT `transdetail_ibfk_2` FOREIGN KEY (`idproduk`) REFERENCES `produk` (`idproduk`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`idstore`) REFERENCES `store` (`idstore`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
