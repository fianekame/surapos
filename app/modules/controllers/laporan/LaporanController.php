<?php

use \modules\controllers\MainController;

class LaporanController extends MainController {

  public function index() {
    $this->model('transaksi');

    $data =$this->transaksi->getWhere(array(
        'kodekasir' => $_SESSION["kodekasir"]
    ));
    $this->template('kasir/transaksi', array('transaksi' => $data));
    // $_SESSION["kodekas"] = $user[0]->hakases;
  }

  public function cekGrafik() {
    $this->model('kasir');
    $weekdata =$this->kasir->getWhere(array(), 'tglbuka > date_sub(now(), interval 1 week)');

    $this->model('transaksi');
    $top5 =$this->kasir->customSql("SELECT transaksidetail.idproduk, SUM(transaksidetail.qty) AS jml,produk.hargajual, produk.namaproduk FROM transaksidetail, produk WHERE produk.idproduk = transaksidetail.idproduk AND MONTH(transaksidetail.tgltrans) = MONTH(CURRENT_DATE())  GROUP BY idproduk ORDER BY jml DESC limit 5");

    $harian =$this->kasir->customSql("SELECT * FROM kasir WHERE MONTH(tglbuka) = MONTH(CURRENT_DATE()) and statuskasir = 1 ORDER BY tglbuka ASC");

    $this->template('laporan/grafik', array('weekdata' => $weekdata, 'top5' => $top5, 'harian' => $harian));
    // $_SESSION["kodekas"] = $user[0]->hakases;
  }

  public function cekProduk() {
    $this->model('transaksi');

    // $data =$this->transaksi->getWhere(array(
    //     'kodekasir' => $_SESSION["kodekasir"]
    // ));
    $this->template('laporan/produk');
    // $_SESSION["kodekas"] = $user[0]->hakases;
  }

  public function cekLaporan() {
    $data = null;
    $this->model('transaksi');
    if($_SERVER["REQUEST_METHOD"] == "POST") {
      $tgl  = isset($_POST["tgl"]) ? $_POST["tgl"] : "";
      $data =$this->transaksi->getWhere(array(),
          " tglorder like '".$tgl."%'"
      );
    }

    $this->template('laporan/penjualan', array('transaksi' => $data));
  }


}
?>
