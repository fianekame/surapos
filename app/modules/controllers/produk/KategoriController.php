<?php

use \modules\controllers\MainController;

class KategoriController extends MainController {

  public function index() {
    $this->model('kategori');
    $data = $this->kategori->get();
    $this->template('produk/kategori', array('kats' => $data));
  }

  public function delete() {
      $id = isset($_GET["id"]) ? $_GET["id"] : 0;
      $this->model('kategori');
      $delete = $this->kategori->delete(array('idkategori' => $id));
      if($delete) {
          $this->back();
      }
  }

  public function addchange() {
      $this->model('kategori');
      $error      = array();
      $success    = null;
      if($_SERVER["REQUEST_METHOD"] == "POST") {
          $id = isset($_POST["id"]) ? $_POST["id"] : "";
          $nama  = isset($_POST["nama"]) ? $_POST["nama"] : "";
          $keterangan = isset($_POST["keterangan"]) ? $_POST["keterangan"] : "";
          if (empty($id)) {
            echo "ADD";
            if(count($error) == 0) {
                $insert = $this->kategori->insert(
                    array(
                      'namakategori' => $nama,
                      'keterangan' => $keterangan
                    )
                );
                if($insert) {
                    $success = "Data Berhasil di ditambahkan.";
                }
            }
          } else {
            $updateArrayData = array(
              'namakategori' => $nama,
              'keterangan' => $keterangan
            );
            if(count($error) == 0) {
                $update = $this->kategori->update($updateArrayData, array('idkategori' => $id));
                if($update) {
                    $success = "Data berhasil di rubah.";
                }
            }
          }
      }
      $this->back();
      // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
  }
}
?>
