<?php

use \modules\controllers\MainController;

class ProdukController extends MainController {

  public function index() {
      $this->model('produk');
      $data = $this->produk->getJoin(array('kategori'),
          array(
            'produk.idkategori' => 'kategori.idkategori'
          ),
          'JOIN'
      );
      $this->model('kategori');
      $kat = $this->kategori->get();
      $this->template('produk/produk', array('produk' => $data, 'kategori' => $kat));
  }

  public function delete() {
    $id = isset($_GET["id"]) ? $_GET["id"] : 0;
    $this->model('produk');
    $delete = $this->produk->delete(array('idproduk' => $id));
    unlink('../assets/produk/' . $_GET["img"]);
    if ($delete) {
        $this->back();
    }
  }

  public function addchange() {
      $this->model('produk');
      $error      = array();
      $success    = null;
      if($_SERVER["REQUEST_METHOD"] == "POST") {
          $id = isset($_POST["id"]) ? $_POST["id"] : "";
          $nama     = isset($_POST["nama"])? $_POST["nama"]   : "";
          $idkat     = isset($_POST["kategori"])? $_POST["kategori"]   : "";
          $hargaproduksi     = isset($_POST["hargaproduksi"])? $_POST["hargaproduksi"]   : "";
          $hargajual     = isset($_POST["hargajual"])? $_POST["hargajual"]   : "";
          $foto     = isset($_FILES["foto"])      ? $_FILES["foto"]    : "";
          $keterangan     = isset($_POST["keterangan"])? $_POST["keterangan"]   : "";
          if (empty($id)) {
            if(count($error) == 0) {
                $imageName = $foto["name"];
                if($foto["name"]) {
                    $imageName = date("h_i_s_Y_m_d_") . str_replace(" ","_", $nama) . '.jpg';
                    move_uploaded_file($foto["tmp_name"], '../assets/produk/' . $imageName);
                }else {
                  $imageName = "default_menu_poto.jpg";
                }
                $insert = $this->produk->insert(
                    array(
                        'namaproduk'       => $nama,
                        'idkategori'       => $idkat,
                        'hargaproduksi'      => $hargaproduksi,
                        'hargajual'  => $hargajual,
                        'gambar'  => $imageName,
                        'keteranganproduk'     => $keterangan
                    )
                );
                if($insert) {
                    $success = "Data Berhasil di ditambahkan.";
                }
            }
          } else {
            $imageName = $foto["name"];
            $dataUpdate = array(
              'namaproduk'       => $nama,
              'idkategori'       => $idkat,
              'hargaproduksi'      => $hargaproduksi,
              'hargajual'  => $hargajual,
              'keteranganproduk'     => $keterangan
            );
            if ($foto["name"]) {
                $imageName = date("h_i_s_Y_m_d_") . str_replace(" ", "_", $nama) . '.jpg';
                // unlink('../assets/produk/' . $data[0]->gambar);
                move_uploaded_file($foto["tmp_name"], '../assets/produk/' . $imageName);
                $dataUpdate = array(
                  'namaproduk'       => $nama,
                  'idkategori'       => $idkat,
                  'hargaproduksi'      => $hargaproduksi,
                  'hargajual'  => $hargajual,
                  'gambar'  => $imageName,
                  'keteranganproduk'     => $keterangan
            );
            }
            $update = $this->produk->update($dataUpdate, array('idproduk' => $id));
            if ($update) {
                $success = "Data Store Berhasil Dirubah.";
            }
          }
      }
      $this->back();
      // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
  }
}
?>
