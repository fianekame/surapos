<?php

use \modules\controllers\MainController;

class KelolaController extends MainController {

    public function index() {
      $this->model('user');
      $data = $this->user->get();
      $this->template('store/kelola', array('users' => $data));
    }

    public function setting(){
      $this->model('user');
      $error      = array();
      $success    = null;
      $id =  $_SESSION["login"]->uniqid;
      $data = $this->user->getWhere(array(
          'uniqid' => $id
      ));
      if(count($data) == 0) $this->redirect(PATH);
      if($_SERVER["REQUEST_METHOD"] == "POST"){
          $psd       = isset($_POST["psdlama"])  ? $_POST["psdlama"] : "";
          $psdbaru1  = isset($_POST["psdbaru1"]) ? $_POST["psdbaru1"] : "";
          $psdbaru2  = isset($_POST["psdbaru2"]) ? $_POST["psdbaru2"] : "";
          if(empty($psd) || $psd == "") {
              array_push($error, "Password Lama wajib di isi.");
          }
          if(empty($psdbaru1) || $psdbaru1 == "") {
              array_push($error, "Password Baru wajib di isi.");
          }
          if(empty($psdbaru2) || $psdbaru2 == "") {
              array_push($error, "Pasword Baru Ulang wajib di isi.");
          }
          if (md5($psd) != $data[0]->password) {
            array_push($error, "Pasword lama tidak sesuai dengan sebelumnya");
          }else{
            if($psdbaru1 != $psdbaru2) {
                array_push($error, "Pasword Baru Tidak Cocok.");
            }
          }
          $updateArrayData = array(
              'password' => md5($psdbaru1)
          );

          if(count($error) == 0) {
              $update = $this->user->update($updateArrayData, array('uniqid' => $id));
              if($update) {
                  $success = "Password berhasil di rubah.";
              }
          }
      }
      $this->template('frmsetting', array('user' => $data[0],'error' => $error, 'success' => $success, 'title' => 'Kelola Password'));
    }

    public function resetPassword() {
        $id = isset($_GET["id"]) ? $_GET["id"] : 0;
        $this->model('user');
        $update = $this->user->update(array('password' => md5("12345")), array('id' => $id));
        if($update) {

          $this->back();
        }
    }
    public function delete()
    {
        $id = isset($_GET["id"]) ? $_GET["id"] : 0;
        $this->model('user');
        $delete = $this->user->delete(array('id' => $id));
        if ($delete) {
            $this->back();
        }
    }

    public function addchange() {
        $this->model('user');
        $error      = array();
        $success    = null;
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $id = isset($_POST["id"]) ? $_POST["id"] : "";
            if (empty($id)) {
              $nama  = isset($_POST["nama"]) ? $_POST["nama"] : "";
              $username  = isset($_POST["username"]) ? $_POST["username"] : "";
              $jabatan  = isset($_POST["jabatan"]) ? $_POST["jabatan"] : "";
              $hakakses = isset($_POST["hakakses"]) ? $_POST["hakakses"] : "";
              if(count($error) == 0) {
                  $insert = $this->user->insert(
                      array(
                        'nama' => $nama,
                        'username' => $username,
                        'jabatan' => $jabatan,
                        'password' => md5($username),
                        'hakakses' => $hakakses
                      )
                  );
                  if($insert) {
                      $success = "Data Berhasil di ditambahkan.";
                  }
              }
            }else{
              $nama  = isset($_POST["nama"]) ? $_POST["nama"] : "";
              $username  = isset($_POST["username"]) ? $_POST["username"] : "";
              $jabatan  = isset($_POST["jabatan"]) ? $_POST["jabatan"] : "";
              $hakakses = isset($_POST["hakakses"]) ? $_POST["hakakses"] : "";
                $updateArrayData = array(
                  'nama' => $nama,
                  'username' => $username,
                  'jabatan' => $jabatan,
                  'hakakses' => $hakakses
                );
                if(count($error) == 0) {
                    $update = $this->user->update($updateArrayData, array('id' => $id));
                    if($update) {
                        $success = "Data berhasil di rubah.";
                    }
                }
            }

        }
        // $this->template('bahan/jenis', array('error' => $error, 'success' => $success,'title' => 'Tambah Jenis Bahan'));
        $this->back();

    }



}
?>
