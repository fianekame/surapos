<?php

use \modules\controllers\MainController;

class KategoriController extends MainController {

  public function index() {
    public function index() {
      $this->model('kategori');
      $data = $this->kategori->get();
      $this->template('store/kelola', array('users' => $data));
    }
  }

  public function delete() {
      $id = isset($_GET["id"]) ? $_GET["id"] : 0;
      $this->model('jenisbahan');
      $delete = $this->jenisbahan->delete(array('idjenisbahan' => $id));
      if($delete) {
          $this->back();
      }
  }

  public function addchange() {
      $ids = $_SESSION['selectedstore'];
      $this->model('jenisbahan');
      $error      = array();
      $success    = null;

      if($_SERVER["REQUEST_METHOD"] == "POST") {
          $id = isset($_POST["id"]) ? $_POST["id"] : "";
          if (empty($id)) {
            $jenisbahan  = isset($_POST["jenisbahan"]) ? $_POST["jenisbahan"] : "";
            $keterangan = isset($_POST["keterangan"]) ? $_POST["keterangan"] : "";
            if(empty($jenisbahan) || $jenisbahan == "") {
                array_push($error, "Jenis Bahan wajib di isi.");
            }
            if(empty($keterangan) || $keterangan == "") {
                array_push($error, "Keterangan wajib di isi.");
            }
            if(count($error) == 0) {
                $insert = $this->jenisbahan->insert(
                    array(
                      'idstore' => $ids,
                      'namajenis' => $jenisbahan,
                      'keteranganjenisbahan' => $keterangan
                    )
                );
                if($insert) {
                    $success = "Data Berhasil di ditambahkan.";
                }
            }
          }else{
            $jenisbahan  = isset($_POST["jenisbahan"]) ? $_POST["jenisbahan"] : "";
            $keterangan = isset($_POST["keterangan"]) ? $_POST["keterangan"] : "";
            if(empty($jenisbahan) || $jenisbahan == "") {
                array_push($error, "Jenis Bahan wajib di isi.");
            }
            if(empty($keterangan) || $keterangan == "") {
                array_push($error, "Keterangan wajib di isi.");
            }
              $updateArrayData = array(
                'namajenis' => $jenisbahan,
                'keteranganjenisbahan' => $keterangan
              );
              if(count($error) == 0) {
                  $update = $this->jenisbahan->update($updateArrayData, array('idjenisbahan' => $id));
                  if($update) {
                      $success = "Data berhasil di rubah.";
                  }
              }
          }

      }
      // $this->template('bahan/jenis', array('error' => $error, 'success' => $success,'title' => 'Tambah Jenis Bahan'));
      $this->back();

  }

}
?>
