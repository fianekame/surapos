<?php

use \modules\controllers\MainController;

class DashboardController extends MainController {

    public function index() {
      $selectedstore = isset($_SESSION["selectedstore"]) ? $_SESSION["selectedstore"] : "";
      $this->template('store/dashboard', array());
    }

}
?>
