<?php

use \modules\controllers\MainController;

class HomeController extends MainController {

    public function index() {
        $this->model('transaksi');
        if( !isset($_SESSION["kodekasir"])){
          $data = array();
          // $this->redirect(PATH."?page=kasir-kasir");
        }else {
          $data =$this->transaksi->getWhere(array(
              'kodekasir' => $_SESSION["kodekasir"]
          ));
        }
        $this->template('home', array('transaksi' => $data));
    }
}
?>
