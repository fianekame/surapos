<?php

use \modules\controllers\MainController;

class TransaksiController extends MainController {

  public function index() {
    $this->model('transaksi');
    if( !isset($_SESSION["kodekasir"]) ){
      $this->redirect(PATH."?page=kasir-kasir");
    }else {
      $data =$this->transaksi->getWhere(array(
          'kodekasir' => $_SESSION["kodekasir"]
      ));
      $this->template('kasir/transaksi', array('transaksi' => $data));
    }
    // $_SESSION["kodekas"] = $user[0]->hakases;
  }


}
?>
