<?php

use \modules\controllers\MainController;

class KasirController extends MainController {

  public function index() {
    $this->model('kasir');
    $data =$this->kasir->getWhere(array(
        'statuskasir' => "0"
    ));
    if (empty($data)) {
      $this->template('kasir/kasir', array('kasir' => $data));
    }else {
      $_SESSION["kodekasir"] = $data[0]->kodekasir;
      $this->redirect(PATH."?page=kasir-pos");
    }
    // $_SESSION["kodekas"] = $user[0]->hakases;
  }

  public function laporan() {
    $this->model('kasir');
    $data =$this->kasir->get();
    $this->template('kasir/laporan', array('kasir' => $data));
  }

  public function detaillaporan() {
    $id = isset($_GET["id"]) ? $_GET["id"] : 0;
    $this->model('transaksi');
    $data =$this->transaksi->getWhere(array(
        'kodekasir' => $id
    ));
    $this->template('kasir/detaillaporan', array('transaksi' => $data));

  }

  public function bukaKasir() {
    $this->model('kasir');
    $hari =  date("Y-m-d H:i:s");
    $kode = str_replace(' ', '', $hari); // Replaces all spaces with hyphens.
    $kode = preg_replace('/[^A-Za-z0-9]/', '', $kode); // Removes special chars
    if($_SERVER["REQUEST_METHOD"] == "POST") {
        $id = isset($_POST["id"]) ? $_POST["id"] : "";
        $jumlah  = isset($_POST["jmlmodal"]) ? $_POST["jmlmodal"] : "";
        if (empty($id)) {
          $insert = $this->kasir->insert(
              array(
                'kodekasir' => $kode,
                'modal' => $jumlah,
                'tglbuka' => $hari
              )
          );
          if($insert) {
              $success = "Data Berhasil di ditambahkan.";
          }
        }
    }
    $_SESSION["kodekasir"] = $kode;
    $this->back();
  }

  public function delete() {
      $id = isset($_GET["id"]) ? $_GET["id"] : 0;
      $this->model('kategori');
      $delete = $this->kategori->delete(array('idkategori' => $id));
      if($delete) {
          $this->back();
      }
  }

}
?>
