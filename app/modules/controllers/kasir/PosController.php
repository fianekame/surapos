<?php

use \modules\controllers\MainController;

class PosController extends MainController {

  public function index() {

    $inv = isset($_GET["id"]) ? $_GET["id"] : 0;
    $trans = NULL;
    $this->model('kasir');
    $kasir =$this->kasir->getWhere(array(
        'statuskasir' => "0"
    ));
    $_SESSION["kodekasir"] = $kasir[0]->kodekasir;

    $this->model('kategori');
    $data =$this->kategori->get();
    $this->model('produk');
    $data1 = $this->produk->getJoin(array('kategori'),
        array(
          'produk.idkategori' => 'kategori.idkategori'
        ),
        'JOIN'
    );
    if ($inv==0) {
      $this->model('transaksi');
      $lastid =$this->transaksi->getLastId()[0];
      $inv = $lastid->AUTO_INCREMENT;
    }else{
      $this->model('transaksi');
      $trans =$this->transaksi->getWhere(array(
          'idtransaksi' => $inv
      ));
    }
    $this->template('kasir/pos', array('kasir' => $kasir,'kategori' => $data,'produk' => $data1, "inv" => $inv, "trans" => $trans));
  }

  public function delete() {
      $id = isset($_GET["id"]) ? $_GET["id"] : 0;
      $this->model('transtemp');
      $delete = $this->transtemp->delete(array('idordertemp' => $id));
      if($delete) {
          $this->back();
      }
  }

  public function tutup() {
      $kodekasir = $_SESSION['kodekasir'];
      $this->model('kasir');
      $total = $this->kasir->customSql("select sum(total) as hasil from transaksi where kodekasir = '".$kodekasir."'");
      // print_r($total);
      $update = $this->kasir->update(array('tgltutup' => date("Y-m-d H:i:s"), 'statuskasir' => 1, 'totalpendapatan' => $total[0]->hasil), array('kodekasir' => $kodekasir));
      if($update) {
        unset($_SESSION['kodekasir']);
        // $this->redirect(PATH."report/CetakClosingan.php?&&id=".$kodekasir);
        $this->redirect(PATH);
      }
  }

  public function simpantransaksi() {
      // $id = isset($_GET["id"]) ? $_GET["id"] : 0;
      $idpetugas =  $_SESSION["login"]->id;
      $status =  1;
      $idtrans  = isset($_POST["idtrans"]) ? $_POST["idtrans"] : "";
      $ada  = isset($_POST["jumlahbarang"]) ? $_POST["jumlahbarang"] : "";
      $sbtotal  = isset($_POST["sbtotal"]) ? $_POST["sbtotal"] : "";
      $kodekasir  = isset($_POST["kodekasir"]) ? $_POST["kodekasir"] : "";
      $inv  = isset($_POST["inv"]) ? $_POST["inv"] : "";
      $nomeja  = isset($_POST["nomeja"]) ? $_POST["nomeja"] : "";
      $nama  = isset($_POST["nama"]) ? $_POST["nama"] : "";
      $this->model('transaksi');

      if (empty($ada)) {
        echo "<script type='text/javascript'>alert('Tidak Bisa Menyimpan Karena Keranjang Belanja Kosong');</script>";
        $this->back();
      }else {
        $insert = $this->transaksi->insert(
            array(
              'idtransaksi' => $idtrans,
              'idpetugas' => $idpetugas,
              'invoice' => $inv,
              'kodekasir' => $kodekasir,
              'nomeja' => $nomeja,
              'atasnama' => $nama,
              'status' => $status,
              'discount' => 0,
              'discountpercent' => 0,
              'tax' => 0,
              'jmlbayar' => 0,
              'totalbelanja' => $sbtotal,
              'total' => 0,
              'keterangan' => " "
            ), True
        );
        $this->redirect(PATH."?page=kasir-transaksi");
      }


  }

  public function bayartransaksi() {
      $this->model('transaksi');
      $error      = array();
      $success    = null;
      if($_SERVER["REQUEST_METHOD"] == "POST") {
          $idpetugas =  $_SESSION["login"]->id;
          $status =  0;
          $kodekasir  = isset($_POST["kodekasirm"]) ? $_POST["kodekasirm"] : "";
          $inv  = isset($_POST["idtransmodal"]) ? $_POST["idtransmodal"] : "";
          $nomeja  = isset($_POST["nomejam"]) ? $_POST["nomejam"] : "";
          $nama  = isset($_POST["namam"]) ? $_POST["namam"] : "";

          $total  = isset($_POST["total"]) ? $_POST["total"] : "";
          // jumlahbelanja
          $totaltanpapajak  = isset($_POST["totaltanpapajak"]) ? $_POST["totaltanpapajak"] : "";
          // dis persen
          $disc  = isset($_POST["discm"]) ? $_POST["discm"] : "0";
          // dis total
          $discount = (int)($disc)/100*((int)$totaltanpapajak);
          // $tax
          $tax = ((int)$totaltanpapajak - $discount) * 0.1;


          $jmlbayar  = isset($_POST["jmlbayarm"]) ? $_POST["jmlbayarm"] : "";
          $kembalian  = isset($_POST["kembalianm"]) ? $_POST["kembalianm"] : "";
          $catatan  = isset($_POST["catatanm"]) ? $_POST["catatanm"] : "";

          if(count($error) == 0) {
              $insert = $this->transaksi->insert(
                  array(
                    'idtransaksi' => $inv,
                    'idpetugas' => $idpetugas,
                    'invoice' => "INV-RM-".$inv,
                    'kodekasir' => $kodekasir,
                    'nomeja' => $nomeja,
                    'atasnama' => $nama,
                    'status' => $status,
                    'discount' => $discount,
                    'discountpercent' => $disc,
                    'tax' => $tax,
                    'jmlbayar' => $jmlbayar,
                    'totalbelanja' => $totaltanpapajak,
                    'total' => $total,
                    'keterangan' => $catatan
                  ), True
              );
              if($insert) {
                  $success = "Data Berhasil di ditambahkan.";
                  $this->model('transaksi');
                  $move =$this->transaksi->moveDetail($inv);
              }
              $this->redirect(PATH."report/CetakOrder.php?&&idt=".$inv);

          }
      }
      // $this->template('frmgudang', array('error' => $error, 'success' => $success,'title' => 'Tambah Bahan Gudang'));
  }
}
?>
