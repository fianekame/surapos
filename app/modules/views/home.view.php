<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
  <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
</div>

<h2>Selamat Datang Di Aplikasi Kasir Suramadu Resto</h2>
<p>
  Gunakan Menu Disamping Untuk Mulai Menggunakan Aplikasi
</p>

<?php if ($_SESSION["type"]==2): ?>
<hr>
<div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Data User</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">Untuk Kelola User</div>
            <hr>
            <a href="<?php echo PATH; ?>?page=store-kelola" class="btn btn-success btn-icon-split">
              <span class="text">Menu User</span>
            </a>
          </div>
          <div class="col-auto">
            <i class="fas fa-user fa-2x text-gray-300"></i>

          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Data Kategori</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">Untuk Kelola Kategori</div>
            <hr>
            <a href="<?php echo PATH; ?>?page=produk-kategori" class="btn btn-success btn-icon-split">
              <span class="text">Menu Kategori</span>
            </a>
          </div>
          <div class="col-auto">
            <i class="fas fa-archive fa-2x text-gray-300"></i>

          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-warning shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Data Menu</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">Untuk Kelola Menu</div>
            <hr>
            <a href="<?php echo PATH; ?>?page=produk-produk" class="btn btn-success btn-icon-split">
              <span class="text">Menu Produk</span>
            </a>
          </div>
          <div class="col-auto">
            <i class="fas fa-coffee fa-2x text-gray-300"></i>

          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-danger shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Data Kasir</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">Untuk Kelola Kasir</div>
            <hr>
            <a href="<?php echo PATH; ?>?page=kasir-kasir" class="btn btn-success btn-icon-split">
              <span class="text">Menu Transaksi</span>
            </a>
          </div>
          <div class="col-auto">
            <i class="fas fa-coffee fa-2x text-gray-300"></i>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
<hr>
<?php if (empty($_SESSION["kodekasir"])): ?>
  <div class="alert alert-warning" role="alert">
  <h4 class="alert-heading">Belum Ada Kasir Yang Terbuka!</h4>
  <p>Mulai Dengan Menu Kasir Di Samping Untuk Mulai Mengelola Resto Anda</p>
  <hr>
  <p class="mb-0">Detail Penggunaan Menu Anda Dapat Temukan Di Buku Manual.</p>
</div>
<?php else: ?>
  <h1 class="h3 mb-2 text-gray-800">Transaksi Hari Ini</h1>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Data Transaksi</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <a href="<?php echo PATH; ?>?page=kasir-pos" class="btn btn-primary btn-icon-split btn-sm" type="button">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Pergi Ke Aplikasi Kasir</span>
        </a>
        <hr>
        <p></p>
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode Kasir</th>
              <th>No Invoice</th>
              <th>No Meja</th>
              <th>Atas Nama</th>
              <th>Total Belanja</th>
              <th>Status</th>
              <th>Tindakan</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>No</th>
              <th>Kode Kasir</th>
              <th>No Invoice</th>
              <th>No Meja</th>
              <th>Atas Nama</th>
              <th>Total Belanja</th>
              <th>Status</th>
              <th>Tindakan</th>
            </tr>
          </tfoot>
          <tbody>
            <?php $no=1; ?>
            <?php foreach ($data["transaksi"] as $data): ?>
              <tr>
                <td>
                  <?php echo $no; ?>
                </td>
                <td>
                  <?php echo $data->kodekasir; ?>
                </td>
                <td>
                  <?php echo $data->invoice; ?>
                </td>
                <td>
                  <?php echo $data->nomeja; ?>
                </td>
                <td>
                  <?php echo $data->atasnama; ?>
                </td>
                <td>
                  <?php echo $data->totalbelanja; ?>
                </td>
                <td>
                  <?php if ($data->status == 0): ?>
                    <a href="#" class="badge badge-primary">Sudah Bayar</a>
                  <?php else: ?>
                    <a href="#" class="badge badge-warning">Belum Bayar</a>

                  <?php endif; ?>
                </td>
                <td>
                  <?php if ($data->status == 0): ?>
                    <a href="" data-toggle="modal" data-target="#formModal" data-id="<?php echo $data->idtransaksi; ?>" class="btn btn-primary btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                          <i class="fas fa-info"></i>
                        </span>
                        <span class="text">Lihat Detail</span>
                    </a>
                  <?php else: ?>
                    <a href="<?php echo SITE_URL; ?>?page=kasir-pos&&id=<?php echo $data->idtransaksi; ?>" class="btn btn-warning btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                          <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Buka Kasir</span>
                    </a>
                  <?php endif; ?>
                </td>
              </tr>
            <?php $no++; endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="label"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="myForm" role="form" action="<?php echo PATH; ?>?page=produk-kategori&&action=addchange" method="post">
            <div class="detailcart">

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
              <a type="button" href="<?php echo PATH; ?>report/CetakOrder.php?&&idt=<?php echo $data->idtransaksi; ?>" class="btn btn-primary">Cetak</a>
            </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript">
      $(document).ready(function(){
          $('#formModal').on('show.bs.modal', function (e) {
              var rowdata = $(e.relatedTarget).data('id');
              if (typeof rowdata !== 'undefined') {
                // document.getElementById('label').innerHTML = "No Invoice : INV-RM-"+rowdata;
                var data = {idt : rowdata};
                $.ajax({
                  type: 'GET',
                  url: "utils/pos/GetDetailChart.php",
                  data: data,
                  success: function(response) {
                    $('.detailcart').html(response);
                  }
                });
              }

           });
      });
    </script>
<?php endif; ?>
