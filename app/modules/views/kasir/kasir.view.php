<pre>
</pre>

<?php if (empty($data["kasir"])): ?>
  <h1 class="h3 mb-2 text-gray-800">Belum Ada Kasir Dibuka</h1>
  <hr>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Buka Kasir</h6>
    </div>
    <div class="card-body">
      <form action="<?php echo PATH; ?>?page=kasir-kasir&&action=bukaKasir" method="post">
        <div class="form-group">
          <label for="formGroupExampleInput2">Jumlah Modal Awal Kasir</label>
          <input type="hidden" name="jmlmodal" id="jmlmodal">
          <input type="text" id="rupiah" name="modal" class="form-control" id="formGroupExampleInput2" placeholder="Masukkan Jumlah">
        </div>
        <button type="submit" class="btn btn-primary btn-lg btn-block">Buka Kasir</button>
      </form>
    </div>
  </div>
<?php else: ?>
  <meta http-equiv="refresh" content="1;url=<?php echo PATH; ?>?page=kasir-pos">
<?php endif; ?>

<script type="text/javascript">

  var rupiah = document.getElementById("rupiah");
  var jmlmodal = document.getElementById("jmlmodal");
  rupiah.addEventListener("keyup", function(e) {
    jmlmodal.value = this.value.replace(/[^0-9]/g, '');
    rupiah.value = formatRupiah(this.value, "Rp. ");
  });

  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, "").toString(),
    split = number_string.split(","),
    sisa = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }

    rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
    return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
  }

</script>
