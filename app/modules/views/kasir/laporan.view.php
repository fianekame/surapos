<h1 class="h3 mb-2 text-gray-800">Laporan Kasir</h1>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Data Kasir</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Kode Kasir</th>
            <th>Tanggal Buka</th>
            <th>Jam Buka</th>
            <th>Tanggal Tutup</th>
            <th>Jam Tutup</th>
            <th>Status Sekarang</th>
            <th>Modal</th>
            <th>Total Pendapatan</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Kode Kasir</th>
            <th>Tanggal Buka</th>
            <th>Jam Buka</th>
            <th>Tanggal Tutup</th>
            <th>Jam Tutup</th>
            <th>Status Sekarang</th>
            <th>Modal</th>
            <th>Total Pendapatan</th>
            <th>Tindakan</th>
          </tr>
        </tfoot>
        <tbody>
          <?php $no=1; ?>
          <?php foreach ($data["kasir"] as $data): ?>
            <tr>
              <td>
                <?php echo $no; ?>
              </td>
              <td>
                <?php echo $data->kodekasir; ?>
              </td>
              <td>
                <?php echo date('d-m-Y',strtotime($data->tglbuka)); ?>
              </td>
              <td>
                <?php echo date('H:i:s',strtotime($data->tglbuka)); ?>
              </td>
              <td>
                <?php echo date('d-m-Y',strtotime($data->tgltutup)); ?>
              </td>
              <td>
                <?php echo date('H:i:s',strtotime($data->tgltutup)); ?>
              </td>
              <td>
                <?php if ($data->statuskasir == 0): ?>
                  <a href="#" class="badge badge-primary">Aktif</a>
                <?php else: ?>
                  <a href="#" class="badge badge-warning">Telah Tutup</a>
                <?php endif; ?>
              </td>
              <td>
                <?php echo $data->modal; ?>
              </td>
              <td>
                <?php echo rupiah($data->totalpendapatan); ?>
              </td>
              <td>
                <a href="<?php echo SITE_URL; ?>?page=kasir-kasir&&action=detaillaporan&&id=<?php echo $data->kodekasir; ?>" class="btn btn-primary btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-info"></i>
                    </span>
                    <span class="text">Lihat Detail</span>
                </a>
              </td>
            </tr>
          <?php $no++; endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
