<pre>
</pre>
<?php if (!empty($data["kasir"])): ?>
  <div class="row">
    <div class="col-lg-7">
      <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-primary">Daftar Menu</h6>
          <div class="dropdown no-arrow">
            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
              <div class="dropdown-header">Ubah Tampilan</div>
              <a class="dropdown-item" name="kartu" onclick="changeView(this)">Menu Kartu</a>
              <a class="dropdown-item" name="tabel" onclick="changeView(this)">Menu Tabel</a>
            </div>
          </div>
        </div>
        <!-- Card Body -->
          <div style="min-height:850px;" class="card-body">
          <div id="kartu" class="">
            <div class="row">
              <div class="form-group col-md-12">
                <form class="formkat" method="post">
                  <input type="hidden" name="inv" value="<?php echo $data['inv']; ?>">
                  <select id="idkategori" name="idkategori" class="form-control">
                    <option value="0" selected>Semua Kategori</option>
                    <?php foreach ($data["kategori"] as $datanya): ?>
                      <option value="<?php echo $datanya->idkategori; ?>"><?php echo $datanya->namakategori; ?></option>
                    <?php endforeach; ?>
                  </select>
                </form>
              </div>
            </div>
            <div class="kategorifilter row pl-2">
              <?php foreach ($data["produk"] as $datanya): ?>
                <div class="card m-1" style="width: 9.2rem; display: inline-block">
                  <img class="card-img-top" src="../assets/produk/<?php echo $datanya->gambar; ?>" alt="Card image cap">
                  <div class="card-body">
                    <h6><?php echo $datanya->namaproduk; ?><a class="stretched-link" style="font-size: 0px;color:red;"><?php echo $datanya->idproduk."-".$data['inv']; ?></a></h6>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>

          <div id="tabel" class="" style="displsy:none;">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nama Menu</th>
                      <th>Kategori</th>
                      <th>Harga</th>
                      <th>Tindakan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($data["produk"] as $datanya): ?>
                      <?php $datas = $datanya->idproduk."-".$data['inv']; ?>
                      <tr>
                        <td>
                          <?php echo $datanya->namaproduk; ?>
                        </td>
                        <td>
                          <?php echo $datanya->namakategori; ?>
                        </td>
                        <td>
                          <?php echo $datanya->hargajual; ?>
                        </td>
                        <td>
                          <a href="#" name="<?php echo $datas; ?>" onclick="addCart(this)" class="btn btn-primary btn-icon-split btn-sm">
                              <span class="icon text-white-50">
                                <i class="fas fa-plus"></i>
                              </span>
                              <span class="text">Tambahkan</span>
                          </a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
          </div>

        </div>
        </div>
      </div>
    </div>
    <div class="col-lg-5">
      <form class="forminv" method="post" action="<?php echo PATH; ?>?page=kasir-pos&&action=simpantransaksi">
      <input type="hidden" name="jumlahbarang" id="jumlahbarang" class="form-control" required>
      <div class="row">
        <div class="col-lg-12">
          <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Detail Transaksi</h6>
            </div>
            <!-- Card Body -->
            <div style="min-height:400px;" class="card-body">
                <div class="form-row">
                  <div class="col">
                    <input type="hidden" name="idtrans" id="idtrans" value="<?php echo $data['inv']; ?>">
                    <input type="text" name="kodekasir" id="kodekasir" class="form-control" value="<?php echo $_SESSION['kodekasir']; ?>" readonly>
                  </div>
                  <div class="col">
                    <input type="text" name="inv" id="inv" class="form-control" value="<?php echo "INV-RM-".$data['inv']; ?>" readonly>
                  </div>
                </div>
                <hr>
                <div class="form-row">
                  <div class="col-lg-3">
                    <input type="number" name="nomeja" id="nomeja" class="form-control" placeholder="No Meja" <?php if(isset($data["trans"])) echo 'value="' . $data["trans"][0]->nomeja . '"'; ?> required>
                  </div>
                  <div class="col">
                    <input type="text" name="nama" id="nama" class="form-control" placeholder="Atas Nama" <?php if(isset($data["trans"])) echo 'value="' . $data["trans"][0]->atasnama . '"'; ?> required>
                  </div>
                </div>
                <hr>
                <div class="cartview" style="height:320px; overflow-y: scroll;">

                </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12">
          <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Total Transaksi <small>Harga Tidak Termasuk PPn (10%)</small></h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
              <input type="text" name="sbtotal" id="sbtotal" class="form-control form-control-lg" readonly>
            </div>
          </div>
          <hr>
        </div>
        <div class="col-lg-12 text-center">
          <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Tindakan Transaksi</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <!-- got to modal -->
                  <a href="" data-toggle="modal" data-target="#formModal" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Bayar Pesanan</span>
                  </a>
                </div>
                <div class="col">
                  <button type="submit" class="btn btn-info btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span class="text">Simpan Pesanan</span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    </div>


  </div>
<?php else: ?>
  <meta http-equiv="refresh" content="1;url=<?php echo PATH; ?>?page=kasir-kasir">
<?php endif; ?>

<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Bayar Pesanan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm" class="myForm" action="<?php echo PATH; ?>?page=kasir-pos&&action=bayartransaksi" method="post">
          <input type="hidden" id="idtransmodal" value="<?php echo $data['inv']; ?>" name="idtransmodal" >
          <div class="form-row">
            <div class="form-group col-md-5">
              <label for="inputEmail4">Kode Kasir</label>
              <input class="form-control" type="text" id="kodekasirm" value="<?php echo $_SESSION['kodekasir']; ?>" name="kodekasirm" readonly>
            </div>
            <div class="form-group col-md-5">
              <label for="inputPassword4">No Invoice</label>
              <input class="form-control" type="text" id="invm" value="<?php echo "INV-RM-".$data['inv']; ?>" name="invm" readonly>
            </div>
            <div class="form-group col-md-2">
              <label for="inputPassword4">No Meja</label>
              <input class="form-control" type="text" id="nomejam" value="" name="nomejam" required>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="inputEmail4">Nama Pembeli</label>
              <input class="form-control" type="text" id="namam" value="" name="namam" required>
            </div>
            <div class="form-group col-md-10">
              <label for="inputEmail4">Total Belanja</label>
              <input id="totalm" type="text" class="form-control" id="inputEmail4" readonly>
              <input class="form-control" type="hidden" id="totaltanpapajak" value="" name="totaltanpapajak" >
            </div>
            <div class="form-group col-md-2">
              <label for="inputEmail4">Diskon</label>
              <input id="discm" name="discm" maxlength="2" type="text" class="form-control" value="0">
            </div>
            <div class="form-group col-md-12">
              <label for="inputEmail4">Subtotal Setelah Diskon + Pajak 10%</label>
              <input class="form-control" type="text" id="total" value="" name="total"  readonly>
            </div>
          </div>
          <hr>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputPassword4">Jumlah Bayar</label>
              <input class="form-control" type="text" id="jmlbayarm" value="" name="jmlbayarm" required>
            </div>
            <div class="form-group col-md-6">
              <label for="inputEmail4">Kembalian</label>
              <input id="kembalianm" name="kembalianm" type="text" class="form-control" readonly>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="inputEmail4">Catatan</label>
              <input id="catatanm" name="catatanm" type="text" class="form-control">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" id="btnsubmit" class="btn btn-block btn-primary">Bayar</button>
          </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  function addCart(elem){
    var data = $(elem).attr("name").split("-");
    var datanya = {idp : data[0], idt : data[1]};
    // console.log(datanya);
    $.ajax({
      type: 'POST',
      url: "utils/pos/AddCart.php",
      data: datanya,
      success: function(response) {
        $('.cartview').load("utils/pos/GetCart.php?id="+data[1]);
        $('#sbtotal').val(bla);
      }
    });
  }
</script>

<script type="text/javascript">
function changeView(elem){
  var nm = $(elem).attr("name");
  if (nm=="kartu") {
    document.getElementById("tabel").style.display = "none";
    document.getElementById("kartu").style.display = "block";
  }else{
    document.getElementById("tabel").style.display = "block";
    document.getElementById("kartu").style.display = "none";
  }
}
</script>
<script type="text/javascript">
$(document).ready(function(){

  $('.cartview').load("utils/pos/GetCart.php?id="+<?php echo $data['inv'];?>);

  document.getElementById("kartu").style.display = "none";

  $('#formModal').on('shown.bs.modal', function (e) {
    $('#idtransmodal').val($('#idtrans').val());
    $('#nomejam').val($('#nomeja').val());
    $('#namam').val($('#nama').val());
    $('#totalm').val(parseInt($('#sbtotal').val()));
    $('#totaltanpapajak').val(parseInt($('#sbtotal').val()));
    $('#total').val(parseInt($('#sbtotal').val()) + (parseInt($('#sbtotal').val())*0.1));
  })

  $('#jmlbayarm').on('input',function(e){
    // console.log(($('#jmlbayarm').val()).toLocaleString());
    $("#btnsubmit").prop("disabled", true);
    $('#kembalianm').val(($('#jmlbayarm').val() - $('#total').val()).toLocaleString());
    if ((parseInt($('#jmlbayarm').val()) < parseInt($('#total').val())) || $('#jmlbayarm').val() == "") {
      $("#btnsubmit").prop("disabled", true);
    }else {
      $("#btnsubmit").removeAttr('disabled');
    }
    // var data = $('.myForm').serialize();
    // console.log(data);

  });

  $('#discm').on('input',function(e){
    var hargdiskon = $('#totaltanpapajak').val() - ($('#totaltanpapajak').val() * ($('#discm').val()/100));
    $('#total').val(hargdiskon + (hargdiskon*0.1));
  });

  $('#idkategori').on('change', function (e) {
    var data = $('.formkat').serialize();
    // var data = {idkategori : 1, idka : 2};
    // console.log(data);
    $.ajax({
      type: 'POST',
      url: "utils/pos/PosChangeKategori.php",
      data: data,
      success: function(response) {
        $('.kategorifilter').html(response);
      }
    });
  });

  $('.stretched-link').click(function(){
    var data = $(this).text().split("-");
    var datanya = {idp : data[0], idt : data[1]};
    // console.log(datanya);
    $.ajax({
    	type: 'POST',
      url: "utils/pos/AddCart.php",
    	data: datanya,
    	success: function(response) {
    		$('.cartview').load("utils/pos/GetCart.php?id="+data[1]);
        $('#sbtotal').val(bla);
    	}
    });
  });



});
</script>
