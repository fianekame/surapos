<!-- Header -->
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Bahan</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="index.php"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item active" aria-current="page"><?php echo $data["title"] ?></li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <!-- Table -->
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header">
          <h3 class="mb-0"><?php echo $data["title"] ?></h3>
          <p class="text-sm mb-3">
            This is an exmaple of datatable using the well known datatables.net plugin. This is a minimal setup in order to get started fast.
          </p>
          <button data-toggle="modal" data-target="#formModal" class="btn btn-icon btn-primary" type="button">
            <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
            <span class="btn-inner--text">Tambah Bahan Bar</span>
          </button>
        </div>
        <div class="table-responsive py-4">
          <table class="table table-flush" id="datatable-basic">
            <thead class="thead-light">
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Jenis</th>
                <th>Stok</th>
                <th>Satuan</th>
                <th>Keterangan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Jenis</th>
                <th>Stok</th>
                <th>Satuan</th>
                <th>Keterangan</th>
                <th>Aksi</th>
              </tr>
            </tfoot>
            <tbody>
              <?php $i = 1; foreach ($data["bahan"] as $bahan): ?>
                <?php $datas = $bahan->idbahan."/".$bahan->idjenisbahan."/".$bahan->namabahan."/".$bahan->stokbahan."/".$bahan->satuan."/".$bahan->keteranganbahan; ?>
                <tr>
                  <td>
                    <?php echo $i; ?>
                  </td>
                  <td>
                    <?php echo $bahan->namabahan; ?>
                  </td>
                  <td>
                    <?php echo $bahan->namajenis; ?>
                  </td>
                  <td>
                    <?php echo $bahan->stokbahan; ?>
                  </td>
                  <td>
                    <?php echo $bahan->satuan; ?>
                  </td>
                  <td>
                    <?php echo $bahan->keteranganbahan; ?>
                  </td>
                  <td class="table-actions">
                    <a href="" data-toggle="modal" data-target="#formModal" data-id="<?php echo $datas; ?>" class="table-action" data-toggle="tooltip" data-original-title="Edit Bahan Gudang">
                      <i class="fas fa-user-edit"></i>
                    </a>
                    <a href="<?php echo SITE_URL; ?>?page=bahan-bar&&action=delete&&id=<?php echo $bahan->idbahan; ?>" onclick="return confirm('Data Akan Di Hapus ?');"  class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Hapus Bahan Gudang">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                </tr>
              <?php $i++; endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Insert -->
  <!-- Modal -->
  <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Bahan Bar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form role="form" id="myForm" action="<?php echo PATH; ?>?page=bahan-bar&&action=addchange" method="post">
            <div class="form-row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-control-label" for="validationDefault01">Nama Bahan</label>
                  <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-control-label" for="validationDefault02">Jenis Bahan</label>
                  <select name="idjenis" id="idjenis" class="form-control">
                    <?php foreach ($data["jenis"] as $jen): ?>
                      <option id="<?php echo $jen->idjenisbahan; ?>" value="<?php echo $jen->idjenisbahan; ?>"><?php echo $jen->namajenis; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-control-label" for="validationDefault02">Satuan Bahan</label>
                  <input type="text" name="satuan" id="satuan" class="form-control"  placeholder="Liter/Gram Dll" required>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-6">
                <input type="hidden" name="id" id="id" class="form-control" placeholder="Id">
                <div class="form-group">
                  <label class="form-control-label" for="validationDefault01">Stok Bahan</label>
                  <input type="number" name="stok" id="stok" class="form-control" placeholder="Stok Sesuai Satuan" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-control-label" for="validationDefault02">Keterangan</label>
                  <input type="text" name="keterangan" id="keterangan" class="form-control"  placeholder="Keterangan" required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#formModal').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            if (typeof rowdata !== 'undefined') {
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              document.getElementById('idjenis').value=data[1];
              document.getElementById('nama').value= data[2];
              document.getElementById('stok').value= data[3];
              document.getElementById('satuan').value= data[4];
              document.getElementById('keterangan').value= data[5];
            }else{
              document.getElementById("myForm").reset();
            }

         });
    });
  </script>
