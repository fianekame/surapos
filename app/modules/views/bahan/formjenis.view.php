<!-- Header -->
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0"><?php echo $data["title"] ?></h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="<?php echo PATH; ?>?page=store-jenis">Jenis Bahan</a></li>
              <li class="breadcrumb-item active" aria-current="page"><?php echo $data["title"] ?></li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">

    <!-- Page Content  -->
    <div class="col">
      <div class="card-wrapper">
        <!-- Default browser form validation -->
        <div class="card">
          <!-- Card header -->
          <div class="card-header">
            <h3 class="mb-0"><?php echo $data["title"] ?></h3>
          </div>
          <!-- Card body -->
          <div class="card-body">
            <div class="row">
              <div class="col-lg-8">
                <p class="mb-0">
                  Not interested in custom validation feedback messages or writing JavaScript to change form behaviors? All good, you can use the browser defaults. Try submitting the form below. Depending on your browser and OS, you’ll
                  see a slightly different style of feedback.
                </p>
              </div>
            </div>
            <hr />
            <!-- Message  -->
            <?php if (isset($data["error"]) && count($data["error"]) > 0) {?>
              <div class="alert alert-solid alert-danger" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php
                  foreach ($data["error"] as $error) {
                      ?>
                      <?php echo "| ".$error; ?>
                  <?php
                } ?>
              </div>
            <?php } elseif (isset($data["success"])) {?>
              <div class="alert alert-solid alert-success">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $data["success"]; ?>
              </div>
              <meta http-equiv="refresh" content="1;url=<?php echo PATH; ?>?page=bahan-jenis">
            <?php } ?>

            <form role="form" enctype="multipart/form-data" class="forms-sample" method="post">
              <div class="form-row">
                <div class="col-md-4 mb-3">
                  <div class="form-group">
                    <label class="form-control-label" for="validationDefault01">Nama Barang</label>
                    <input type="text" name="nama" <?php if(isset($data["barang"])) echo 'value="' . $data["barang"]->namabarang . '"'; ?>  class="form-control" placeholder="Nama Barang"required>
                  </div>
                </div>
                <div class="col-md-4 mb-3">
                  <div class="form-group">
                    <label class="form-control-label" for="validationDefault02">Lokasi Barang</label>
                    <input type="text" name="lokasi" <?php if(isset($data["barang"])) echo 'value="' . $data["barang"]->lokasi . '"'; ?>  class="form-control"  placeholder="Belakang" required>
                  </div>
                </div>
              </div>
              <button class="btn btn-lg btn-block btn-primary" type="submit">Simpan Data</button>

            </form>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
