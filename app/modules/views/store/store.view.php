<?php
$selectedstore = isset($_SESSION["selectedstore"]) ? $_SESSION["selectedstore"] : "";
?>
<div class="header pb-6 d-flex align-items-center" style="min-height: 400px; background-image: url(../resource/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;">
  <!-- Mask -->
  <span class="mask bg-gradient-default opacity-8"></span>
  <!-- Header container -->
  <div class="container-fluid d-flex align-items-center">
    <div class="row">
      <div class="col-lg-7 col-md-10">
        <h1 class="display-2 text-white">Welcome,</h1>
        <p class="text-white mt-0 mb-2">This is your profile page. You can see the progress you've made with your work and manage your projects or assigned tasks</p>
        <a href="<?php echo PATH; ?>?page=store-store&&action=newstore" class="btn btn-neutral">Buat Toko Baru</a>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12 order-xl-1">
      <div class="row">
        <?php foreach ($data["mystore"] as $store): ?>
          <div class="col-xl-4">
            <div class="card card-profile">
              <img src="../resource/img/theme/img-1-1000x600.jpg" alt="Image placeholder" class="card-img-top">
              <div class="row justify-content-center">
                <div class="col-lg-3 order-lg-2">
                  <div class="card-profile-image">
                    <a href="#">
                      <img src="../assets/store/<?php echo $store->logo; ?>" class="rounded-circle">
                    </a>
                  </div>
                </div>
              </div>
              <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                <div class="d-flex justify-content-between">
                  <?php if ($selectedstore==$store->idstore): ?>
                    <button type="button" name="<?php echo $store->idstore."-".$store->namastore; ?>" class="disbutton btn btn-sm btn-danger mr-4">Disabel Store</button>
                  <?php else: ?>
                    <button type="button" name="<?php echo $store->idstore."-".$store->namastore; ?>" class="enbutton btn btn-sm btn-info mr-4">Activate Store</button>
                  <?php endif; ?>
                  <div class="float-right">
                    <a href="<?php echo SITE_URL; ?>?page=store-store&&action=update&&id=<?php echo $store->idstore; ?>" class="btn btn-sm btn-default ">Edit</a>
                    <a href="<?php echo SITE_URL; ?>?page=store-store&&action=delete&&id=<?php echo $store->idstore; ?>&&img=<?php echo $store->logo ?>" onclick="return confirm('Data Akan Di Hapus ?');" class="btn btn-sm btn-warning ">Delete</a>
                  </div>
                </div>
              </div>
              <div class="card-body pt-0">
                <div class="row">
                  <div class="col">
                    <div class="card-profile-stats d-flex justify-content-center">
                      <div>
                        <span class="heading">22</span>
                        <span class="description">Transaksi</span>
                      </div>
                      <div>
                        <span class="heading">10</span>
                        <span class="description">Pekerja</span>
                      </div>
                      <div>
                        <span class="heading">89</span>
                        <span class="description">Pelanggan</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="text-center">
                  <h5 class="h3">
                    <?php echo $store->namastore; ?>
                  </h5>
                  <div class="h5 font-weight-300">
                    <i class="ni location_pin mr-2"></i><?php echo $store->alamat; ?> , <?php echo $store->alamat; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</div>

<script>
$('.enbutton').on('click', function(e){
  var name = $(this).attr('name');
  console.log(name);
  $.ajax({
      type: 'POST',
      url: 'library/sessionstore.php',
      data: {
        selectedstore: name
      }
  });
  location.reload();
});
$('.disbutton').on('click', function(e){
  var name = "";
  $.ajax({
      type: 'POST',
      url: 'library/sessionstore.php',
      data: {
        selectedstore: name
      }
  });
  location.reload();
});
</script>
