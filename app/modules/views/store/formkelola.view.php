
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Tambah Data User</h1>
<p class="mb-4">Kelola Data Pengguna, Administrator Maupun Kasir.</p>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Tambah Data User</h6>
  </div>
  <div class="card-body">
    <form role="form" enctype="multipart/form-data" class="forms-sample" method="post">
      <div class="form-row">
        <div class="col-md-6 mb-3">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Nama Depan</label>
            <input type="text" name="nadep" <?php if(isset($data["user"])) echo 'value="' . $data["user"]->nadep . '"'; ?>  class="form-control" placeholder="Nama Depan"required>
          </div>
        </div>
        <div class="col-md-6 mb-3">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault02">Username</label>
            <input type="text" name="nabel" <?php if(isset($data["user"])) echo 'value="' . $data["user"]->nabel . '"'; ?>  class="form-control"  placeholder="Belakang" required>
          </div>
        </div>
      </div>
      <div class="form-row">
        <div class="col-md-4 mb-3">
          <div class="form-group">
            <label for="exampleFormControlSelect1">Example select</label>
            <select class="form-control" id="exampleFormControlSelect1">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
        </div>
        <div class="col-md-4 mb-3">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault03">Jabatan</label>
            <input type="text" name="jabatan" <?php if(isset($data["user"])) echo 'value="' . $data["user"]->jabatan . '"'; ?> class="form-control"  placeholder="Dapat Disesuaikan" required>
          </div>
        </div>
      </div>
      <button class="btn btn-primary" type="submit">Simpan Data</button>
    </form>
  </div>
</div>
