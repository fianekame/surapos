<!-- Header -->
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Buat Toko Baru</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="<?php echo PATH; ?>?page=store-store">Store</a></li>
              <li class="breadcrumb-item active" aria-current="page"><?php echo $data["title"] ?></li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">

    <!-- Page Content  -->
    <div class="col">
      <div class="card-wrapper">
        <!-- Default browser form validation -->
        <div class="card">
          <!-- Card header -->
          <div class="card-header">
            <h3 class="mb-0">Tambah Toko Baru</h3>
          </div>
          <!-- Card body -->
          <div class="card-body">
            <div class="row">
              <div class="col-lg-8">
                <p class="mb-0">
                  Not interested in custom validation feedback messages or writing JavaScript to change form behaviors? All good, you can use the browser defaults. Try submitting the form below. Depending on your browser and OS, you’ll
                  see a slightly different style of feedback.
                </p>
              </div>
            </div>
            <hr />
            <!-- Message  -->
            <?php if (isset($data["error"]) && count($data["error"]) > 0) {?>
              <div class="alert alert-solid alert-danger" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php
                  foreach ($data["error"] as $error) {
                      ?>
                      <?php echo "| ".$error; ?>
                  <?php
                } ?>
              </div>
            <?php } elseif (isset($data["success"])) {?>
              <div class="alert alert-solid alert-success">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <?php echo $data["success"]; ?>
              </div>
              <meta http-equiv="refresh" content="1;url=<?php echo PATH; ?>?page=store-store">
            <?php } ?>

            <form role="form" enctype="multipart/form-data" class="forms-sample" method="post">
              <div class="form-row">
                <div class="col-md-4 mb-3">
                  <div class="form-group">
                    <label class="form-control-label" for="validationDefault01">Nama Toko</label>
                    <input type="text" name="nama" <?php if(isset($data["store"])) echo 'value="' . $data["store"]->namastore . '"'; ?> class="form-control" placeholder="Ex : Kopiku"required>
                  </div>
                </div>
                <div class="col-md-4 mb-3">
                  <div class="form-group">
                    <label class="form-control-label" for="validationDefault02">Nomor Telpon</label>
                    <input type="text" name="telpon" <?php if(isset($data["store"])) echo 'value="' . $data["store"]->phone . '"'; ?> class="form-control"  placeholder="Ex : 082xxxxx" required>
                  </div>
                </div>
                <div class="col-md-4 mb-3">
                  <div class="form-group">
                    <label class="form-control-label" for="validationDefault02">Alamat</label>
                    <input type="text" name="alamat" <?php if(isset($data["store"])) echo 'value="' . $data["store"]->alamat . '"'; ?> class="form-control"  placeholder="Jln. Contoh No 07" required>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-6 mb-3">
                  <div class="form-group">
                    <label class="form-control-label" for="validationDefault03">Keterangan</label>
                    <input type="text" name="keterangan" <?php if(isset($data["store"])) echo 'value="' . $data["store"]->keteranganstore . '"'; ?> class="form-control"  placeholder="Warung Ini Adalah Warung Pertama" required>
                  </div>
                </div>
                <div class="col-md-3 mb-3">
                  <div class="form-group">
                    <label class="form-control-label" for="validationDefault03">Logo Toko</label>
                    <div class="custom-file">
                      <input type="file" <?php if(isset($data["store"])) echo 'value="' . $data["store"]->logo . '"'; ?> name="foto" class="custom-file-input" id="customFileLang" lang="en">
                      <label class="custom-file-label" for="customFileLang">Select file</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 mb-3">
                  <div class="form-group">
                    <label class="form-control-label" for="validationDefault03">Cover Toko</label>
                    <div class="custom-file">
                      <input type="file" name="cover" <?php if(isset($data["store"])) echo 'value="' . $data["store"]->cover . '"'; ?> class="custom-file-input" id="customFileLang" lang="en">
                      <label class="custom-file-label" for="customFileLang">Select file</label>
                    </div>
                  </div>
                </div>
              </div>
              <button class="btn btn-primary" type="submit">Buat Toko Baru</button>
            </form>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
