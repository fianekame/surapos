<!-- <pre> -->
  <!-- <?php
    print_r($data["users"]);
  ?> -->
<!-- </pre> -->
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Kelola Data User</h1>
<p class="mb-4">Kelola Data Pengguna, Administrator Maupun Kasir.</p>


<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Data User</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <button data-toggle="modal" data-id="add" data-target="#formModal" class="btn btn-primary btn-icon-split btn-sm" type="button">
        <span class="icon text-white-50">
          <i class="fas fa-plus"></i>
        </span>
        <span class="text">Tambah Data</span>
      </button>
      <hr>
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>Username</th>
            <th>Hak Akses</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>Username</th>
            <th>Hak Akses</th>
            <th>Tindakan</th>
          </tr>
        </tfoot>
        <tbody>
          <?php foreach ($data["users"] as $team): ?>
            <?php $datas = $team->id."/".$team->nama."/".$team->username."/".$team->jabatan."/".$team->hakakses; ?>
            <tr>
              <td>
                <?php echo $team->nama; ?>
              </td>
              <td>
                <?php echo $team->jabatan; ?>
              </td>
              <td>
                <?php echo $team->username; ?>
              </td>
              <td>
                <?php if ($team->hakakses=="1"): ?>
                  Kasir
                <?php else: ?>
                  Administrator
                <?php endif; ?>
              </td>
              <td>
                <a href="" data-toggle="modal" data-target="#formModal" data-id="<?php echo $datas; ?>" class="btn btn-primary btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-wrench"></i>
                    </span>
                    <span class="text">Edit</span>
                </a>
                <a onclick="return confirm('Password Akan Di Reset ?');" href="<?php echo SITE_URL; ?>?page=store-kelola&&action=resetPassword&&id=<?php echo $team->id; ?>" class="btn btn-warning btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-retweet"></i>
                    </span>
                    <span class="text">Reset</span>
                </a>
                <a href="<?php echo SITE_URL; ?>?page=store-kelola&&action=delete&&id=<?php echo $team->id; ?>" onclick="return confirm('Data Akan Di Hapus ?');" class="btn btn-danger btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Hapus</span>
                </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah User Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm" role="form" action="<?php echo PATH; ?>?page=store-kelola&&action=addchange" method="post">
          <div class="form-row">
            <div class="col-md-12">
              <input type="hidden" name="id" id="id" class="form-control" placeholder="Id">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Nama</label>
                <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Username</label>
                <input type="text" name="username" id="username" class="form-control" placeholder="Username" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Jabatan</label>
                <input type="text" name="jabatan" id="jabatan" class="form-control" placeholder="Jabatan" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault02">Hak Akses</label>
                <select name="hakakses" id="hakakses" class="form-control" id="exampleFormControlSelect1">
                  <option value="2">Administrator</option>
                  <option value="1">Kasir</option>
                </select>
              </div>
            </div>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#formModal').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            } else {
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              document.getElementById('nama').value= data[1];
              document.getElementById('username').value= data[2];
              document.getElementById('jabatan').value= data[3];
              document.getElementById('hakakses').value= data[4];
            }
         });
    });
  </script>
