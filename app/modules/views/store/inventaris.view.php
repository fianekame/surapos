<!-- Header -->
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Kelola Store</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="index.php"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="<?php echo PATH; ?>?page=store-store">My Store</a></li>
              <li class="breadcrumb-item active" aria-current="page"><?php echo $data["title"] ?></li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Page content -->
<div class="container-fluid mt--6">
  <!-- Table -->
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header">
          <h3 class="mb-0"><?php echo $data["title"] ?></h3>
          <p class="text-sm mb-3">
            This is an exmaple of datatable using the well known datatables.net plugin. This is a minimal setup in order to get started fast.
          </p>
          <a href="<?php echo PATH; ?>?page=store-inventaris&&action=insert" class="btn btn-icon btn-primary" type="button">
            <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
            <span class="btn-inner--text">Tambah Barang</span>
          </a>
        </div>
        <div class="table-responsive py-4">
          <table class="table table-flush" id="datatable-basic">
            <thead class="thead-light">
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Lokasi</th>
                <th>Kondisi</th>
                <th>Jumlah</th>
                <th>Aksi</th>
                <th>Keterangan</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Lokasi</th>
                <th>Kondisi</th>
                <th>Jumlah</th>
                <th>Aksi</th>
                <th>Keterangan</th>
              </tr>
            </tfoot>
            <tbody>
              <?php $i = 1; foreach ($data["barang"] as $data): ?>
                <tr>
                  <td>
                    <?php echo $i; ?>
                  </td>
                  <td>
                    <?php echo $data->namabarang; ?>
                  </td>
                  <td>
                    <?php echo $data->lokasi; ?>
                  </td>
                  <td>
                    <?php echo $data->kondisi; ?>
                  </td>
                  <td>
                    <?php echo $data->jumlah. " Barang"; ?>
                  </td>
                  <td class="table-actions">
                    <a href="<?php echo SITE_URL; ?>?page=store-inventaris&&action=update&&id=<?php echo $data->idbarang; ?>" class="table-action" data-toggle="tooltip" data-original-title="Edit Barang">
                      <i class="fas fa-user-edit"></i>
                    </a>
                    <a href="<?php echo SITE_URL; ?>?page=store-inventaris&&action=delete&&id=<?php echo $data->idbarang; ?>" onclick="return confirm('Data Akan Di Hapus ?');"  class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Hapus Barang">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                  <td>
                    <?php echo $data->keteranganbarang; ?>
                  </td>
                </tr>
              <?php $i++; endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
