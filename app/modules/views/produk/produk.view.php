<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Kelola Data Menu</h1>
<p class="mb-4">Tambahkan, Ganti, Dan Hapus Data Data Menu.</p>
<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Data Menu</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <button data-toggle="modal" data-id="add" data-target="#formModal" class="btn btn-primary btn-icon-split btn-sm" type="button">
        <span class="icon text-white-50">
          <i class="fas fa-plus"></i>
        </span>
        <span class="text">Tambah Menu</span>
      </button>
      <hr>
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Gambar</th>
            <th>Nama</th>
            <th>Kategori</th>
            <th>Harga Produksi</th>
            <th>Harga Jual</th>
            <th>Keterangan</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>Gambar</th>
            <th>Nama</th>
            <th>Kategori</th>
            <th>Harga Produksi</th>
            <th>Harga Jual</th>
            <th>Keterangan</th>
            <th>Tindakan</th>
          </tr>
        </tfoot>
        <tbody>
          <?php foreach ($data["produk"] as $datanya): ?>
            <?php $datas = $datanya->idproduk."/".$datanya->namaproduk."/".$datanya->gambar."/".$datanya->idkategori."/".$datanya->hargaproduksi."/".$datanya->hargajual."/".$datanya->keteranganproduk; ?>
            <tr valign="center">
              <td align="center">
                <img height="48" width="48" class="rounded-circle" src="../assets/produk/<?php echo $datanya->gambar; ?>">
              </td>
              <td>
                <?php echo $datanya->namaproduk; ?>
              </td>
              <td>
                <?php echo $datanya->namakategori; ?>
              </td>
              <td>
                <?php echo rupiah((int)$datanya->hargaproduksi); ?>
              </td>
              <td>
                <?php echo rupiah((int)$datanya->hargajual); ?>
              </td>
              <td>
                <?php echo $datanya->keteranganproduk; ?>
              </td>
              <td>
                <a href="" data-toggle="modal" data-target="#formModal" data-id="<?php echo $datas; ?>" class="btn btn-primary btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-wrench"></i>
                    </span>
                    <span class="text">Edit</span>
                </a>
                <a href="<?php echo SITE_URL; ?>?page=produk-produk&&action=delete&&id=<?php echo $datanya->idproduk; ?>&&img=<?php echo $datanya->gambar ?>" onclick="return confirm('Data Akan Di Hapus ?');" class="btn btn-danger btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Hapus</span>
                </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Menu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm" enctype="multipart/form-data" role="form" action="<?php echo PATH; ?>?page=produk-produk&&action=addchange" method="post">
          <div class="form-row">
            <div class="col-md-12">
              <input type="hidden" name="id" id="id" class="form-control" placeholder="Id">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Nama Menu</label>
                <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault02">Nama Kategori</label>
                <select name="kategori" id="kategori" class="form-control" id="exampleFormControlSelect1">
                  <?php foreach ($data["kategori"] as $datanya): ?>
                    <option value="<?php echo $datanya->idkategori; ?>"><?php echo $datanya->namakategori; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Upload Gambar Produk</label>
                <div class="input-group col-xs-12">
                  <input type="file" name="foto" id="foto" class="form-control-file" >
                </div>
                  <hr>
                  <img width="48" height="48" id="fotonya" class="rounded-circle img-lg">
                  <small><code>* Biarkan Bila Tidak Ada Perubahan</code></small>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Harga Produksi</label>
                <input type="number" name="hargaproduksi" id="hargaproduksi" class="form-control" placeholder="Harga Produksi" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Harga Jual</label>
                <input type="number" name="hargajual" id="hargajual" class="form-control" placeholder="Harga Jual" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="form-control-label" for="validationDefault01">Keterangan</label>
                <input type="text" name="keterangan" id="keterangan" class="form-control" placeholder="Keeterangan" required>
              </div>
            </div>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#formModal').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            }else{
              var data = rowdata.split("/");
              document.getElementById('nama').value= data[1];
              document.getElementById('fotonya').src= "../assets/produk/"+data[2];
              document.getElementById('kategori').value= data[3];
              document.getElementById('hargaproduksi').value= data[4];
              document.getElementById('hargajual').value= data[5];
              document.getElementById('keterangan').value= data[6];
            }
         });
    });
  </script>
