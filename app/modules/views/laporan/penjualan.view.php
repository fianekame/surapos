<!-- DataTales Example -->
<div class="row">
  <div class="col-lg-3">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Filter Laporan</h6>
      </div>
      <div class="card-body">
        <form class="formtgl" method="post">
          <div class="form-group">
            <label for="formGroupExampleInput">Pilih Tanggal</label>
            <input type="date" id="tgl" name="tgl" class="form-control" placeholder="Example input">
          </div>
          <div class="form-group text-center">
            <button href="#" type="submit" class="btn btn-primary btn-icon-split">
              <span class="icon text-gray-600">
                <i class="fas fa-arrow-right"></i>
              </span>
              <span class="text">Cek Laporan Produk</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-lg-9">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Hasil Laporan</h6>
      </div>
      <div class="card-body">
        <div class="hasildata" style="max-height:720px; overflow-y: scroll;">
          <?php if (empty($data["transaksi"])): ?>
            Tidak Ada Data
          <?php else: ?>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Kasir</th>
                  <th>No Invoice</th>
                  <th>No Meja</th>
                  <th>Atas Nama</th>
                  <th>Total Belanja</th>
                  <th>Status</th>
                  <th>Tindakan</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Kode Kasir</th>
                  <th>No Invoice</th>
                  <th>No Meja</th>
                  <th>Atas Nama</th>
                  <th>Total Belanja</th>
                  <th>Status</th>
                  <th>Tindakan</th>
                </tr>
              </tfoot>
              <tbody>
                <?php $no=1; ?>
                <?php foreach ($data["transaksi"] as $data): ?>
                  <tr>
                    <td>
                      <?php echo $no; ?>
                    </td>
                    <td>
                      <?php echo $data->kodekasir; ?>
                    </td>
                    <td>
                      <?php echo $data->invoice; ?>
                    </td>
                    <td>
                      <?php echo $data->nomeja; ?>
                    </td>
                    <td>
                      <?php echo $data->atasnama; ?>
                    </td>
                    <td>
                      <?php echo $data->total; ?>
                    </td>
                    <td>
                      <?php if ($data->status == 0): ?>
                        <a href="#" class="badge badge-primary">Sudah Bayar</a>
                      <?php else: ?>
                        <a href="#" class="badge badge-warning">Belum Bayar</a>

                      <?php endif; ?>
                    </td>
                    <td>
                      <?php if ($data->status == 0): ?>
                        <a href="" data-toggle="modal" data-target="#formModal" data-id="<?php echo $data->idtransaksi; ?>" class="btn btn-primary btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                              <i class="fas fa-info"></i>
                            </span>
                            <span class="text">Lihat Detail</span>
                        </a>
                      <?php else: ?>
                        <a href="<?php echo SITE_URL; ?>?page=kasir-pos&&id=<?php echo $data->idtransaksi; ?>" class="btn btn-warning btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                              <i class="fas fa-plus"></i>
                            </span>
                            <span class="text">Buka Kasir</span>
                        </a>
                      <?php endif; ?>
                    </td>
                  </tr>
                <?php $no++; endforeach; ?>
              </tbody>
            </table>
          <?php endif; ?>

        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="label"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm" role="form" action="<?php echo PATH; ?>?page=produk-kategori&&action=addchange" method="post">
          <div class="detailcart">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <a type="button" href="<?php echo PATH; ?>report/CetakOrder.php?&&idt=<?php echo $data->idtransaksi; ?>" class="btn btn-primary">Cetak</a>
          </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#formModal').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            if (typeof rowdata !== 'undefined') {
              // document.getElementById('label').innerHTML = "No Invoice : INV-RM-"+rowdata;
              var data = {idt : rowdata};
              $.ajax({
                type: 'GET',
                url: "utils/pos/GetDetailChart.php",
                data: data,
                success: function(response) {
                  $('.detailcart').html(response);
                }
              });
            }

         });
    });
  </script>
