<!-- DataTales Example -->
<div class="row">
  <div class="col-lg-3">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Filter Laporan</h6>
      </div>
      <div class="card-body">
        <form class="formtgl" method="post">
          <div class="form-group">
            <label for="formGroupExampleInput">Pilih Tanggal</label>
            <input type="date" id="tgl" name="tgl" class="form-control" placeholder="Example input">
          </div>
          <div class="form-group text-center">
            <a href="#" onclick="cek(this)" class="btn btn-primary btn-icon-split">
              <span class="icon text-gray-600">
                <i class="fas fa-arrow-right"></i>
              </span>
              <span class="text">Cek Laporan Produk</span>
            </a>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-lg-9">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Hasil Laporan</h6>
      </div>
      <div class="card-body">
        <div class="hasildata" style="max-height:720px; overflow-y: scroll;">

        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
      function cek(elem){
        var data = $('.formtgl').serialize();
        // var data = {idkategori : 1, idka : 2};
        // console.log(data);
        $.ajax({
          type: 'GET',
          url: "utils/pos/LaporanProduk.php",
          data: data,
          success: function(response) {
            $('.hasildata').html(response);
          }
        });
      }
  </script>
