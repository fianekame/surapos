-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 02 Mar 2020 pada 20.55
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sura_pos`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kasir`
--

CREATE TABLE `kasir` (
  `idkasir` int(11) NOT NULL,
  `kodekasir` varchar(256) NOT NULL,
  `tglbuka` timestamp NOT NULL DEFAULT current_timestamp(),
  `tgltutup` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `statuskasir` int(11) NOT NULL DEFAULT 0 COMMENT '0 buka 1 tutup',
  `modal` int(11) NOT NULL,
  `totalpendapatan` int(11) NOT NULL DEFAULT 0 COMMENT 'diupdate saat closingan'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kasir`
--

INSERT INTO `kasir` (`idkasir`, `kodekasir`, `tglbuka`, `tgltutup`, `statuskasir`, `modal`, `totalpendapatan`) VALUES
(1, '20200223203651', '2020-02-23 20:36:51', '0000-00-00 00:00:00', 1, 50000, 0),
(4, '20200224005207', '2020-02-24 00:52:07', '2020-03-03 02:26:43', 1, 50000, 803000),
(5, '20200303025357', '2020-03-03 02:53:57', '0000-00-00 00:00:00', 0, 30000, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `idkategori` int(11) NOT NULL,
  `namakategori` varchar(64) NOT NULL,
  `keterangan` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`idkategori`, `namakategori`, `keterangan`) VALUES
(1, 'Seafood', 'Makanan Berdasatkan Seafood'),
(2, 'Ayam Ayaman', 'Makanan Berdasatkan Ayam Ayam');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `idproduk` int(11) NOT NULL,
  `namaproduk` varchar(256) NOT NULL,
  `gambar` varchar(264) NOT NULL DEFAULT 'default_poto_menu.jpg',
  `idkategori` int(11) NOT NULL,
  `keteranganproduk` varchar(256) NOT NULL,
  `hargaproduksi` varchar(256) NOT NULL,
  `hargajual` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`idproduk`, `namaproduk`, `gambar`, `idkategori`, `keteranganproduk`, `hargaproduksi`, `hargajual`) VALUES
(1, 'Seafod 111', 'default_menu_poto.jpg', 1, 'maknak an saidnaid asdaldha dmakdmakdna jdnaj a', '15000', '25000'),
(2, 'Ayam Ayaman 111', 'default_menu_poto.jpg', 2, 'dasfa fa fahfahf afa f', '17500', '50000'),
(7, 'Seafod 11111', 'default_menu_poto.jpg', 1, 'maknak an saidnaid asdaldha dmakdmakdna jdnaj a', '15000', '25000'),
(8, 'Ayam Ayaman 11111', 'default_menu_poto.jpg', 2, 'dasfa fa fahfahf afa f', '17500', '50000'),
(9, 'Seafod', 'default_menu_poto.jpg', 1, 'maknak an saidnaid asdaldha dmakdmakdna jdnaj a', '15000', '25000'),
(10, 'Ayam Ayaman', 'default_menu_poto.jpg', 2, 'dasfa fa fahfahf afa f', '17500', '50000'),
(11, 'Seafod', 'default_menu_poto.jpg', 1, 'maknak an saidnaid asdaldha dmakdmakdna jdnaj a', '15000', '25000'),
(12, 'Ayam Ayaman', 'default_menu_poto.jpg', 2, 'dasfa fa fahfahf afa f', '17500', '50000'),
(13, 'Menu Baru', 'default_menu_poto.jpg', 2, 'Menu Ini', '25000', '30000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `idtransaksi` int(11) NOT NULL,
  `invoice` varchar(64) NOT NULL,
  `kodekasir` varchar(256) NOT NULL,
  `idpetugas` int(11) NOT NULL,
  `tglorder` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `nomeja` varchar(64) NOT NULL,
  `atasnama` varchar(64) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0 sudah 1 belum',
  `discount` int(11) NOT NULL DEFAULT 0,
  `discountpercent` int(11) NOT NULL DEFAULT 0,
  `tax` int(11) NOT NULL,
  `serviscas` int(11) DEFAULT NULL,
  `jmlbayar` int(11) NOT NULL,
  `totalbelanja` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `keterangan` varchar(264) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`idtransaksi`, `invoice`, `kodekasir`, `idpetugas`, `tglorder`, `nomeja`, `atasnama`, `status`, `discount`, `discountpercent`, `tax`, `serviscas`, `jmlbayar`, `totalbelanja`, `total`, `keterangan`) VALUES
(1, 'INV-RM-1', '', 1, '2020-02-24 13:17:20', '', '', 0, 0, 0, 0, 0, 0, 0, 0, ''),
(2, 'INV-RM-2', '20200224005207', 1, '2020-03-01 18:57:12', '2', 'hahahaha', 0, 0, 0, 15000, NULL, 170000, 150000, 165000, 'hahaha'),
(3, 'INV-RM-3', '20200224005207', 1, '2020-03-01 19:02:44', '3', 'beli lagi', 0, 10000, 10, 10000, NULL, 100000, 100000, 99000, 'huhuhuh'),
(4, 'INV-RM-4', '20200224005207', 1, '2020-03-01 19:08:36', '2', 'abdul', 0, 10000, 10, 9000, NULL, 100000, 100000, 99000, 'tes'),
(8, 'INV-RM-8', '20200224005207', 1, '2020-03-02 15:38:24', '23', 'Sofian', 0, 0, 0, 40000, NULL, 500000, 400000, 440000, ''),
(9, 'INV-RM-9', '20200303025357', 1, '2020-03-02 19:54:08', '12', 'Sofian', 1, 0, 0, 0, NULL, 0, 100000, 0, ' ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksidetail`
--

CREATE TABLE `transaksidetail` (
  `iddetail` int(11) NOT NULL,
  `idtransaksi` int(11) NOT NULL,
  `idproduk` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `tgltrans` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksidetail`
--

INSERT INTO `transaksidetail` (`iddetail`, `idtransaksi`, `idproduk`, `qty`, `tgltrans`) VALUES
(10, 2, 8, 1, '2020-03-02 17:15:03'),
(11, 2, 9, 1, '2020-03-02 17:15:03'),
(12, 2, 12, 1, '2020-03-02 17:15:03'),
(13, 2, 11, 1, '2020-03-02 17:15:03'),
(17, 3, 8, 1, '2020-03-02 17:15:03'),
(18, 3, 9, 1, '2020-03-02 17:15:03'),
(19, 3, 11, 1, '2020-03-02 17:15:03'),
(20, 4, 9, 2, '2020-03-02 17:15:03'),
(21, 4, 8, 1, '2020-03-02 17:15:03'),
(36, 8, 12, 1, '2020-03-02 17:15:03'),
(37, 8, 11, 2, '2020-03-02 17:15:03'),
(38, 8, 10, 6, '2020-03-02 17:15:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksitemp`
--

CREATE TABLE `transaksitemp` (
  `idordertemp` int(11) NOT NULL,
  `idtransaksi` int(11) NOT NULL,
  `idproduct` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `nama` varchar(256) NOT NULL,
  `foto` varchar(256) NOT NULL DEFAULT 'admin.jpg',
  `jabatan` varchar(256) NOT NULL,
  `hakakses` varchar(256) NOT NULL COMMENT '0 tidak ada, 1 kasir, 2 admin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `foto`, `jabatan`, `hakakses`) VALUES
(1, 'admin', '827ccb0eea8a706c4c34a16891f84e7b', 'Admin Suramadu Resto', 'admin.jpg', 'Administrator', '2'),
(2, 'kasir1', '827ccb0eea8a706c4c34a16891f84e7b', 'Kasir Suramadu Resto', 'admin.jpg', 'Kasir ', '1'),
(3, 'kasir2', '827ccb0eea8a706c4c34a16891f84e7b', 'Kasir Suramadu Resto', 'admin.jpg', 'Kasir ', '1');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kasir`
--
ALTER TABLE `kasir`
  ADD PRIMARY KEY (`idkasir`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`idkategori`);

--
-- Indeks untuk tabel `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`idproduk`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`idtransaksi`);

--
-- Indeks untuk tabel `transaksidetail`
--
ALTER TABLE `transaksidetail`
  ADD PRIMARY KEY (`iddetail`);

--
-- Indeks untuk tabel `transaksitemp`
--
ALTER TABLE `transaksitemp`
  ADD PRIMARY KEY (`idordertemp`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kasir`
--
ALTER TABLE `kasir`
  MODIFY `idkasir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `idkategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `produk`
--
ALTER TABLE `produk`
  MODIFY `idproduk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `idtransaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `transaksidetail`
--
ALTER TABLE `transaksidetail`
  MODIFY `iddetail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `transaksitemp`
--
ALTER TABLE `transaksitemp`
  MODIFY `idordertemp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
